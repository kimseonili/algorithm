import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_977_A {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		char[] N = st.nextToken().toCharArray();
		int last = N.length - 1;
		int K = Integer.parseInt(st.nextToken());
		for (int k = 0; k < K; k++) {

			if (N[last] == '0') {
				N[last] = 0;
				last--;
			}

			else {
				N[last]--;
			}

		}
		for (char c : N) {
			if (c != 0)
				bw.write((int) c);
		}
		bw.flush();
	}
}
