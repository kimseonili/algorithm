import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_15654_N��M5 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int N = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		st = new StringTokenizer(br.readLine());
		int arr[] = new int[N];
		for(int  i = 0 ; i < N ; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		Arrays.sort(arr);
		perm(arr, new int[C], 0, 0, C, new boolean[N]);
		bw.flush();
	}
	static BufferedWriter bw;
	static void perm(int arr[], int res[],  int idx,int count, int max, boolean check[]) throws IOException{
		
		if(max== count) {
			
			for(int i : res)
				bw.write(i + " ");
			bw.newLine();
			return;
		}
		
		for(int i = 0; i < arr.length ; i++) {
			if(check[i]) continue;
			res[count] = arr[i];
			check[i] = true;
			perm(arr, res, i+1, count+1, max, check);
			check[i] = false;
			
		}
		
	}
}
