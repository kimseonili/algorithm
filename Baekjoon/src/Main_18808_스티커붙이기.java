import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_18808_스티커붙이기 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());
		int arr[][] = new int[N][M];
		int[][][][] stickers = new int[K][4][][];
		int[] stickerCnt = new int[K];
		for (int k = 0; k < K; k++) {
			st = new StringTokenizer(br.readLine());
			int r = Integer.parseInt(st.nextToken());
			int c = Integer.parseInt(st.nextToken());
			stickers[k][0] = new int[r][c];
			for (int i = 0; i < r; i++) {
				st = new StringTokenizer(br.readLine());
				for (int j = 0; j < c; j++) {
					stickers[k][0][i][j] = Integer.parseInt(st.nextToken());
					if (stickers[k][0][i][j] == 1)
						stickerCnt[k]++;
				}
			}
		}
		for (int k = 0; k < K; k++) {
			for (int i = 1; i < 4; i++) {
				stickers[k][i] = lotate(stickers[k][i - 1]);
			}
		}
		int answer = DFS(arr, stickers, stickerCnt, 0, 0);
		bw.write(answer + "");
		bw.flush();
	}

//	static int max;
	static int DFS(int arr[][], int stickers[][][][], int[] stickerCnt, int stickerIdx, int res) {
//		print(arr);
		if (stickerIdx == stickers.length) {
			return res;
		}

		for (int d = 0; d < 4; d++) {
			// 가능하다면 부착
			for (int i = 0; i < arr.length; i++) {
				for (int j = 0; j < arr[0].length; j++) {
					if (isPaste(i, j, arr, stickers[stickerIdx][d])) {
						pasteSticker(i, j, arr, stickers[stickerIdx][d]);
						return DFS(arr, stickers, stickerCnt, stickerIdx + 1, res + stickerCnt[stickerIdx]);
					}
				}
			}
		}
		return DFS(arr, stickers, stickerCnt, stickerIdx + 1, res);

	}

	static int[][] lotate(int[][] sticker) {
		int[][] lotateSticker = new int[sticker[0].length][sticker.length];

		for (int i = 0; i < lotateSticker.length; i++) {
			for (int j = 0; j < lotateSticker[0].length; j++) {
				lotateSticker[i][j] = sticker[lotateSticker[0].length - 1 - j][i];
			}
		}

		return lotateSticker;
	}

	static boolean isPaste(int r, int c, int arr[][], int[][] sticker) {
		if (r + sticker.length > arr.length || c + sticker[0].length > arr[0].length) {
			return false;
		}
		for (int i = 0; i < sticker.length; i++) {
			for (int j = 0; j < sticker[0].length; j++) {
				if (sticker[i][j] == 0) {
					continue;
				}
				if (arr[r + i][c + j] == 1) {
					return false;
				}

			}

		}

		return true;
	}

	static void pasteSticker(int r, int c, int arr[][], int[][] sticker) {

		for (int i = 0; i < sticker.length; i++) {
			for (int j = 0; j < sticker[0].length; j++) {
				arr[r + i][c + j] = sticker[i][j] == 1 ? 1 : arr[r+i][c+j];
			}
		}

	}

	static int calcMax(int[] stickerCnt, int stickerIdx, int curSum) {
		int res = curSum;
		for (int i = stickerIdx; i < stickerCnt.length; i++) {
			res += stickerCnt[i];
		}
		return res;
	}

	static void print(int arr[][]) {
		for (int i[] : arr)
			System.out.println(Arrays.toString(i));
		System.out.println();
	}
}
