import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_15655_N��M6 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		int arr[] = new int[N];
		st = new StringTokenizer(br.readLine());
		for (int i = 0; i < N; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}

		Arrays.sort(arr);
		perm(arr, new boolean[N], 0, 0, C);

		bw.flush();

	}

	static BufferedWriter bw;

	static void perm(int arr[], boolean check[], int idx, int c, int C) throws IOException {

		if (c == C) {
			for (int i = 0; i < arr.length; i++)
				if (check[i])
					bw.write(arr[i] + " ");
			bw.newLine();
			return;
		}

		if (idx == arr.length)
			return;

		check[idx] = true;
		perm(arr, check, idx + 1, c + 1, C);
		check[idx] = false;
		perm(arr, check, idx + 1, c, C);

	}
}
