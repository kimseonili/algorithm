import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.StringTokenizer;

public class Main_17837_새로운게임2 {
	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());

		int arr[][] = new int[N + 2][N + 2];
		for (int i[] : arr)
			Arrays.fill(i, 2);
		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < N; j++) {
				arr[i + 1][j + 1] = Integer.parseInt(st.nextToken());
			}
		}

		int horse[][] = new int[K][4];

		for (int i = 0; i < K; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < 3; j++) {
				horse[i][j] = Integer.parseInt(st.nextToken());
			}
			horse[i][2]--;
			horse[i][3] = i;
		}
		System.out.println(BFS(arr, horse));

	}

	static int delta[][] = { { 0, 1 }, { 0, -1 }, { -1, 0 }, { 1, 0 } };

	static int BFS(int arr[][], int horse[][]) {

		int N = arr.length;
		Queue<int[]> q = new LinkedList<>();
		LinkedList<int[]> chess[][] = new LinkedList[N][N];
		for (int i[] : horse) {
			int r = i[0];
			int c = i[1];
			chess[r][c] = new LinkedList<>();
			chess[r][c].offer(i);
			q.offer(i);
		}
		int cnt = 0;
		while (cnt < 1000 * horse.length) {

			int cur[] = q.poll();
			int d = cur[2];
			int nr = cur[0] + delta[d][0];
			int nc = cur[1] + delta[d][1];

			if (arr[nr][nc] == 0) {
				move(chess, cur, nr, nc);
				cur[0] = nr;
				cur[1] = nc;
			} else if (arr[nr][nc] == 1) {
				move(chess, cur, nr, nc);
				reverse(chess[cur[0]][cur[1]], cur);
				cur[0] = nr;
				cur[1] = nc;

			} else {
				cur[2] = d % 2 == 0 ? d + 1 : d - 1;
				int br = cur[0] - delta[d][0];
				int bc = cur[1] - delta[d][1];
				if (arr[br][bc] == 1) {
					move(chess, cur, br, bc);
					reverse(chess[cur[0]][cur[1]], cur);
					cur[0] = br;
					cur[1] = bc;
				} else if (arr[br][bc] == 2) {
					// nothing;
				} else {
					move(chess, cur, br, bc);
					cur[0] = br;
					cur[1] = bc;
				}
			}
//			System.out.println(cnt);
//			print(chess, horse);
			if (chess[cur[0]][cur[1]].size() > 3) {
				return cnt / horse.length + 1;
			}
			q.offer(cur);
			cnt++;
		}

		return -1;
	}

	static void move(LinkedList<int[]>[][] chess, int cur[], int nr, int nc) {

		int r = cur[0];
		int c = cur[1];

		if (chess[nr][nc] == null) {
			chess[nr][nc] = new LinkedList<>();
		}
		boolean flag = false;
		Iterator<int[]> it = chess[r][c].iterator();

		while (it.hasNext()) {
			int h[] = it.next();
			if (cur == h || flag) {
				flag = true;
				h[0] = nr;
				h[1] = nc;
				it.remove();
				chess[nr][nc].offer(h);
			}
		}
		if (chess[r][c].size() == 0) {
			chess[r][c] = null;
		}

	}

	static void reverse(LinkedList<int[]> c, int[] cur) {
		Stack<int[]> tmp = new Stack<>();
		Iterator<int[]> it = c.iterator();
		boolean flag = false;
		while (it.hasNext()) {
			int[] h = it.next();
			if (flag || cur == h) {
				flag = true;
				tmp.push(h);
				it.remove();
			}
		}
		while (!tmp.isEmpty()) {
			c.offer(tmp.pop());
		}
	}

	static void print(LinkedList<int[]>[][] chess, int[][] horse) {
		for (LinkedList<int[]> i[] : chess) {
			for (LinkedList<int[]> j : i) {
				if (j == null)
					System.out.print(0 + " ");
				else
					System.out.print(j.size() + " ");
			}
			System.out.println();
		}
		for (int i[] : horse)
			System.out.println(Arrays.toString(i));
		System.out.println();
	}
}
