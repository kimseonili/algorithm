import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_2206_벽부수고이동하기 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		int arr[][] = new int[R][C];

		for (int i = 0; i < R; i++) {
			String input = br.readLine();
			for (int j = 0; j < C; j++) {
				arr[i][j] = input.charAt(j) - '0';
			}
		}
		q.add(new int[] { 0, 0, 0, 1 });
		boolean visit[][][] = new boolean[2][R][C];
		visit[0][0][0] = true;
		BFS(arr, visit);
		bw.write(answer + "");
		bw.flush();
	}

	static int answer = -1;
	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
	static Queue<int[]> q = new LinkedList<>();

	static void BFS(int arr[][], boolean visit[][][]) {

		while (!q.isEmpty()) {
			int cur[] = q.poll();
			if (cur[0] == arr.length - 1 && cur[1] == arr[0].length - 1) {
				answer = cur[3];
				return;
			}
			for (int k = 0; k < 4; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];

				if (isRange(nr, nc, arr.length, arr[0].length) && !visit[cur[2]][nr][nc]) {

					if (arr[nr][nc] == 0) {
						visit[cur[2]][nr][nc] = true;

						q.add(new int[] { nr, nc, cur[2], cur[3] + 1 });
					}

					else if (arr[nr][nc] == 1 && cur[2] == 0) {
						q.add(new int[] { nr, nc, 1, cur[3] + 1 });
						visit[1][nr][nc] = true;
					}

				}
			}

		}
//		for(int i[] : arr)
//			System.out.println(Arrays.toString(i));
//		
//		for(boolean[][] i : visit)
//		{
//			for(boolean j[] : i)
//			System.out.println(Arrays.toString(j));
//			System.out.println();
//		}
	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}
}
