import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_1600_���̵ǰ��¿����� {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st;

		int K = Integer.parseInt(br.readLine());
		st = new StringTokenizer(br.readLine());
		int C = Integer.parseInt(st.nextToken());
		int R = Integer.parseInt(st.nextToken());

		int arr[][] = new int[R][C];

		for (int i = 0; i < R; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < C; j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		BFS(arr, K);
		bw.write(answer + "");
		bw.flush();

	}

	static int answer = Integer.MAX_VALUE;
	static Queue<int[]> q = new LinkedList<>();

	static void BFS(int arr[][], int K) {
		boolean visit[][][] = new boolean[K + 1][arr.length][arr[0].length];
		visit[0][0][0] = true;
		q.add(new int[] { 0, 0, 0, 0 });
		while (!q.isEmpty()) {

			int cur[] = q.poll();
			int cnt = cur[2];
			for (int k = 0; k < delta.length; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];
				if (k == 4) {
					if (cnt == K)
						break;
					else
						cnt++;
				}
				if (isRange(nr, nc, arr.length, arr[0].length) && !visit[cnt][nr][nc] && arr[nr][nc] == 0) {

					if (nr == arr.length - 1 && nc == arr[0].length - 1) {
						if (answer > cur[3] + 1)
							answer = cur[3] + 1;
					}

					if (arr[nr][nc] == 0) {
						visit[cnt][nr][nc] = true;
						q.add(new int[] { nr, nc, cnt, cur[3] + 1 });
					}
				}

			}

		}
		if (answer == Integer.MAX_VALUE)
			answer = -1;
	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 }, { -1, -2 }, { -2, -1 }, { -2, 1 }, { -1, 2 },
			{ 1, -2 }, { 2, -1 }, { 2, 1 }, { 1, 2 } };

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}
}
