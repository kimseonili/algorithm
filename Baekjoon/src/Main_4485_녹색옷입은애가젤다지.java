import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_4485_����������ְ������� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int N;
		int t = 1;
		while ((N = Integer.parseInt(br.readLine())) != 0) {
			int arr[][] = new int[N][N];
			for (int i = 0; i < N; i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				for (int j = 0; j < N; j++) {
					arr[i][j] = Integer.parseInt(st.nextToken());
				}
			}
			int answer = BFS(arr);
			bw.write("Problem " + t++ + ": " + answer);
			bw.newLine();
		}
		bw.flush();
	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static boolean isRange(int nr, int nc, int N) {
		return nr >= 0 && nc >= 0 && nr < N && nc < N;
	}

	static Queue<int[]> q = new LinkedList<>();

	static int BFS(int arr[][]) {
		int min[][] = new int[arr.length][arr.length];
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length; j++) {
				min[i][j] = Integer.MAX_VALUE;
			}
		}
		min[0][0] = arr[0][0];

		q.clear();
		q.add(new int[] { 0, 0, arr[0][0] });

		while (!q.isEmpty()) {

			int cur[] = q.poll();

			for (int k = 0; k < 4; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];
				if (isRange(nr, nc, arr.length) && min[nr][nc] > cur[2] + arr[nr][nc]) {
					min[nr][nc] = arr[nr][nc] + cur[2];
					if (nr == arr.length - 1 && nc == arr.length - 1) {
						continue;
					} else {
						q.add(new int[] {nr,nc,min[nr][nc]});
					}

				}
			}
		}
		return min[arr.length - 1][arr.length - 1];
	}
}
