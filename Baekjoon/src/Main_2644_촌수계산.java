import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_2644_�̼���� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st;

		int N = Integer.parseInt(br.readLine());
		st = new StringTokenizer(br.readLine());
		int from = Integer.parseInt(st.nextToken()) - 1;
		int to = Integer.parseInt(st.nextToken()) - 1;
		int M = Integer.parseInt(br.readLine());
		int arr[][] = new int[N][N];

		for (int i = 0; i < M; i++) {
			st = new StringTokenizer(br.readLine());
			int x = Integer.parseInt(st.nextToken()) - 1;
			int y = Integer.parseInt(st.nextToken()) - 1;
			arr[x][y] = 1;
			arr[y][x] = 1;
		}
		bw.write(Integer.toString(BFS(arr, from, to)));
		bw.flush();
	}

	static int BFS(int arr[][], int from, int to) {
		Queue<int[]> q = new LinkedList<>();
		q.add(new int[] { from, 0 });
		boolean visit[] = new boolean[arr.length];
		visit[from] = true;

		while (!q.isEmpty()) {
			int cur[] = q.poll();
//			System.out.println(Arrays.toString(cur));
			for (int k = 0; k < arr.length; k++) {
				if (visit[k])
					continue;
				if (arr[cur[0]][k] == 1) {
					if (k == to) {
						return cur[1] + 1;
					}
					q.add(new int[] { k, cur[1] + 1 });
					visit[k] = true;
				}
			}
		}

		return -1;

	}
}
