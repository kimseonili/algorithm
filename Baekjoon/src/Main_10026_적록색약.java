import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;

public class Main_10026_���ϻ��� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		
		int N = Integer.parseInt(br.readLine());
		
		char arr[][] = new char[N][N];
		
		for(int i = 0 ; i < N ; i++) {
			arr[i] = br.readLine().toCharArray();
			
		}
		int answerRGB = 0;
		int answer = 0;
		boolean visit[][] = new boolean[N][N];
		Queue<int[]> q = new LinkedList<>();
		int delta[][] = {{-1,0}, {1,0},{0,-1},{0,1}};
		for(int i = 0 ; i < N ; i++) {
			for(int j = 0 ; j < N ; j++) {
				q.clear();
				if(!visit[i][j] ) {
					answer++;
					q.add(new int[] {i,j, arr[i][j]});
					visit[i][j] = true;
					while(!q.isEmpty()) {
						int cur[] = q.poll();
						
						for(int  k = 0 ; k < 4; k++) {
							int nr = cur[0]+delta[k][0];
							int nc = cur[1]+delta[k][1];
							if(isRange(nr,nc,N) && !visit[nr][nc] && cur[2] == arr[nr][nc]) {
								visit[nr][nc] = true;
								q.add(new int[] {nr,nc,cur[2]});
							}
						}
						
					}
				}
				
				
			}
		}
		
		for(int i = 0 ; i < N ; i++) {
			for(int j = 0 ; j < N ; j++) {
				if(arr[i][j] == 'G') {
					arr[i][j] = 'R';
				}
			}
		}
		visit = new boolean[N][N];
		
		for(int i = 0 ; i < N ; i++) {
			for(int j = 0 ; j < N ; j++) {
				q.clear();
				if(!visit[i][j] ) {
					answerRGB++;
					q.add(new int[] {i,j, arr[i][j]});
					visit[i][j] = true;
					while(!q.isEmpty()) {
						int cur[] = q.poll();
						
						for(int  k = 0 ; k < 4; k++) {
							int nr = cur[0]+delta[k][0];
							int nc = cur[1]+delta[k][1];
							if(isRange(nr,nc,N) && !visit[nr][nc] && cur[2] == arr[nr][nc]) {
								visit[nr][nc] = true;
								q.add(new int[] {nr,nc,cur[2]});
							}
						}
						
					}
				}
				
				
			}
		}
		bw.write(answer + " " + answerRGB);
		bw.flush();
	}
	
	static boolean isRange(int nr, int nc, int N) {
		return nr >= 0 && nc >= 0 && nc < N && nr < N;
	}
}
