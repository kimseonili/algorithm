import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_18224_�̷ο������ǿ� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());

		int arr[][] = new int[N][N];

		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < N; j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		String answer = BFS(arr, M);
		bw.write(answer);
		bw.flush();
	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static boolean isRange(int nr, int nc, int N) {
		return nr >= 0 && nc >= 0 && nr < N && nc < N;
	}

	static String BFS(int arr[][], int M) {
		String res = "";

		Queue<int[]> q = new LinkedList<>();
		q.add(new int[] { 0, 0, 0 });
		int n = arr.length;
		int visit[][][] = new int[M * 2][n][n];
		for (int i[][] : visit)
			for (int j[] : i)
				Arrays.fill(j, Integer.MAX_VALUE);
		visit[0][0][0] = 0;
		while (!q.isEmpty()) {
			int cur[] = q.poll();

			if (cur[0] == n - 1 && cur[1] == n - 1) {
				continue;
			}

			for (int k = 0; k < delta.length; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];
				int curTime = (cur[2] / M) % 2;
				int nd = cur[2] + 1;
				if (isRange(nr, nc, n)) {

					if (arr[nr][nc] == 0) {
						if (visit[nd % (M * 2)][nr][nc] > nd) {
							q.add(new int[] { nr, nc, nd });
							visit[nd % (M * 2)][nr][nc] = nd;
						}
					} else {// ���϶�
						if (curTime == 0) {
							continue;// ���� �ƴҶ� �Ұ�
						}
						boolean canGo = true;
						while (arr[nr][nc] == 1) {
							nr += delta[k][0];
							nc += delta[k][1];
							if (!isRange(nr, nc, n)) {
								canGo = false;
								break;
							}
						}
						if (canGo) {
							if (visit[nd % (M * 2)][nr][nc] > nd) {
								visit[nd % (M * 2)][nr][nc] = nd;
								q.add(new int[] { nr, nc, nd });
							}
						}
					}

				}
			}
		}
		int min = visit[0][n - 1][n - 1];
		for (int i = 1; i < M * 2; i++) {
			min = Math.min(min, visit[i][n - 1][n - 1]);
		}
		if (min == Integer.MAX_VALUE)
			return "-1";
		res = (min / (M * 2) + 1) + " ";
		if ((min / M) % 2 == 0) {
			res += "sun";
		} else {
			res += "moon";
		}

		return res;
	}
}
