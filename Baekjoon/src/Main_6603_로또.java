import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main_6603_�ζ� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st;
		String input;
		while (!(input = br.readLine()).equals("0")) {

			st = new StringTokenizer(input);
			int N = Integer.parseInt(st.nextToken());
			int arr[] = new int[N];

			for (int i = 0; i < N; i++) {
				arr[i] = Integer.parseInt(st.nextToken());
			}

			comb(arr, new boolean[N], 0, 0);
			bw.newLine();
		}
		bw.flush();

	}

	static BufferedWriter bw;

	static void comb(int[] arr, boolean check[], int idx, int c) throws IOException {
		if (c == 6) {
			for (int i = 0; i < arr.length; i++)
				if (check[i])
					bw.write(arr[i] + " ");
			bw.newLine();
			return;
		}

		if (idx == arr.length)
			return;

		check[idx] = true;
		comb(arr, check, idx + 1, c + 1);
		check[idx] = false;
		comb(arr, check, idx + 1, c);
	}
}
