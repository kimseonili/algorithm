import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_14719_���� {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		int H = Integer.parseInt(st.nextToken());
		int W = Integer.parseInt(st.nextToken());
		Queue<int[]> q = new LinkedList<>();
		int arr[][] = new int[H][W];
		st = new StringTokenizer(br.readLine());
		for(int i = 0 ; i < W ; i++) {
			int h = Integer.parseInt(st.nextToken());
			for(int j = 0 ; j < h ; j++) {
				arr[H - j - 1][i] = 1; 
				q.offer(new int[] {H - j - 1,i,0});
			}
		}
		bw.write(bfs(arr, q) + "");
		bw.flush();
		
	}
	static int bfs(int arr[][], Queue<int[]> q) {
		int answer = 0;
		while(!q.isEmpty()) {
			int cur[] = q.poll();
			
			int r = cur[0];
			int nc = cur[1] + 1;
			
			if(isRange(nc, arr[0].length)) {
				if(arr[r][nc] == 0) {
					q.add(new int[] {r,nc,cur[2]+1});
				}
				else {
					answer += cur[2];
				}
			}
			
		}
		
		return answer;
	}
	
	static boolean isRange(int nc, int C) {
		return nc < C;
	}
}
