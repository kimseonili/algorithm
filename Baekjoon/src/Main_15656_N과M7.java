import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_15656_N��M7 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		int arr[] = new int[N];
		st = new StringTokenizer(br.readLine());
		for (int i = 0; i < N; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		Arrays.sort(arr);
		comb(arr, new int[C], 0, 0, C);
		bw.flush();
	}

	static BufferedWriter bw;

	static void comb(int arr[], int res[], int idx, int c, int C) throws IOException {
		if (c == C) {

			for (int i : res) {
					bw.write(i + " ");
			}
			bw.newLine();

			return;
		}
		if(idx == arr.length)
			return;
		res[c] = arr[idx];
		comb(arr, res, 0, c+1, C);
		comb(arr, res, idx+1, c, C);
		
		
	}
}
