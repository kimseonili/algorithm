import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main_2164_카드2 {

	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		
		int N = Integer.parseInt(br.readLine());
		int arr[] = new int[N*2];
		
		for(int i = 0 ; i < N ; i++) {
			arr[i] = i+1;
		}
		
		int idx = 0;
		int lastIdx = N;
		while(lastIdx -1 != idx) {
			
			//카드 버리기
			idx++;
			//카드 밑으로 옮기기
			arr[lastIdx++] = arr[idx++];
		}
		bw.write(arr[idx] + "");
		bw.flush();
	}
}
