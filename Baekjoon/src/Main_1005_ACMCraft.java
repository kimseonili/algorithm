import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_1005_ACMCraft {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());

			ArrayList<Integer> arr[] = new ArrayList[N + 1];
			for (int i = 0; i < N; i++) {
				arr[i + 1] = new ArrayList<>();
			}
			int buildings[] = new int[N + 1];
			st = new StringTokenizer(br.readLine());

			for (int i = 1; i < N + 1; i++) {
				buildings[i] = Integer.parseInt(st.nextToken());
			}
			for (int i = 0; i < K; i++) {
				st = new StringTokenizer(br.readLine());
				int a = Integer.parseInt(st.nextToken());
				int b = Integer.parseInt(st.nextToken());
				arr[b].add(a);
			}
			int W = Integer.parseInt(br.readLine());
			int[] dp = new int[N + 1];

			dp[W] = buildings[W];
			BFS(dp, arr, W, buildings);
			int answer = 0;
			for (int i = 1; i < N + 1; i++) {
				answer = Math.max(answer, dp[i]);
			}
			System.out.println(answer);
		}

	}

	static void BFS(int[] dp, ArrayList<Integer> arr[], int idx, int buildings[]) {

		Queue<Integer> q = new LinkedList<>();
		q.add(idx);

		while (!q.isEmpty()) {

			int cur = q.poll();

			for (int i = 0; i < arr[cur].size(); i++) {
				int dest = arr[cur].get(i);
				if (dp[dest] < dp[cur] + buildings[dest]) {
					q.add(dest);
					dp[dest] = dp[cur] + buildings[dest];
				}

			}

		}

	}
}
