import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_1726_�κ� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		int arr[][] = new int[R][C];

		for (int i = 0; i < R; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < C; j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		int robot[][] = new int[2][];
		int visit[][][] = new int[4][R][C];
		for (int i[][] : visit)
			for (int j[] : i)
				Arrays.fill(j, Integer.MAX_VALUE);

		for (int i = 0; i < 2; i++) {
			st = new StringTokenizer(br.readLine());
			int r = Integer.parseInt(st.nextToken()) - 1;
			int c = Integer.parseInt(st.nextToken()) - 1;
			int dir = Integer.parseInt(st.nextToken());
			switch (dir) {
			case 2:
				dir = 3;
				break;
			case 3:
				dir = 2;
				break;
			case 4:
				dir = 0;
				break;
			}

			robot[i] = new int[] { r, c, dir };
		}
//		for(int i[] : robot)
//			System.out.println(Arrays.toString(i));
		BFS(arr, visit, robot);
		bw.write(answer + "");
		bw.flush();
	}

	static int delta[][] = { { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 } };
	static Queue<int[]> q = new LinkedList<>();
	static int answer = Integer.MAX_VALUE;

	static void BFS(int arr[][], int visit[][][], int[][] robot) {

		int startR = robot[0][0];
		int startC = robot[0][1];
		int startDir = robot[0][2];

		int targetR = robot[1][0];
		int targetC = robot[1][1];
		int targetDir = robot[1][2];
		q.add(new int[] { startR, startC, startDir, 0, 0 });
		visit[startDir][startR][startC] = 0;

		while (!q.isEmpty()) {

			int cur[] = q.poll();
			if (cur[0] == targetR && cur[1] == targetC && cur[2] == targetDir) {
				if (answer > cur[3])
					answer = cur[3];
			}

			int dir = cur[2];
			int com = cur[3];
			int nr = cur[0] + delta[dir][0];
			int nc = cur[1] + delta[dir][1];

			if (isRange(nr, nc, arr.length, arr[0].length) && arr[nr][nc] == 0) {

				if ((cur[4] == 0  || cur[4] > 2) && visit[dir][nr][nc] >= com +1) {
					q.add(new int[] { nr, nc, dir, com + 1, 1 });
					visit[dir][nr][nc] = com + 1;
				}
				if (cur[4] > 0 && cur[4] < 3  && visit[dir][nr][nc] >= com) {
					q.add(new int[] { nr, nc, dir, com, cur[4] + 1 });
					visit[dir][nr][nc] = com;
				}

			}
			if (visit[(dir + 1) % 4][cur[0]][cur[1]] >= com + 1) {
				q.add(new int[] { cur[0], cur[1], (dir + 1) % 4, com + 1, 0 });
				visit[(dir + 1) % 4][cur[0]][cur[1]] = com + 1;
			}

			if (visit[(dir + 3) % 4][cur[0]][cur[1]] >= com + 1) {
				q.add(new int[] { cur[0], cur[1], (dir + 3) % 4, com + 1, 0 });
				visit[(dir + 3) % 4][cur[0]][cur[1]] = com + 1;
			}
		}
	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr < R && nc < C && nr >= 0 && nc >= 0;
	}
}
