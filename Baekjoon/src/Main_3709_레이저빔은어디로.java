import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_3709_레이저빔은어디로 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {

			StringTokenizer st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken()) + 2;
			int R = Integer.parseInt(st.nextToken());
			int arr[][] = new int[N][N];
			for (int i = 0; i < R; i++) {
				st = new StringTokenizer(br.readLine());
				int c = Integer.parseInt(st.nextToken());
				int r = N - 1 - Integer.parseInt(st.nextToken());
				arr[r][c] = 1;
			}
			st = new StringTokenizer(br.readLine());

			int c = Integer.parseInt(st.nextToken());
			int r = N - 1 - Integer.parseInt(st.nextToken());
			bw.write(go(arr, r, c));
			bw.newLine();
			arr[r][c] = 2;
			
			
			
//			System.out.println();
//			for (int i[] : arr)
//				System.out.println(Arrays.toString(i));
		}
		bw.flush();
	}

	static boolean isRange(int nr, int nc, int N) {
		return nr >= 0 && nc >= 0 && nr < N && nc < N;
	}

	static int[][] delta = { { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 } };

	static String go(int arr[][], int r, int c) {
		boolean[][][] visit = new boolean[4][arr.length][arr.length];
		int t[][] = new int[arr.length][arr.length];
		int nr = r;
		int nc = c;
		int d = 0;
		if (r == 0) {
			d = 2;
		} else if (c == 0) {
			d = 1;
		} else if (c == arr.length - 1) {
			d = 3;
		} else if (r == arr.length - 1) {
			d = 0;
		}
		int cnt = 1;
		while (isRange(nr, nc, arr.length)) {
			t[nr][nc] = cnt++;
			if(visit[d][nr][nc]) {
				return "0 0";
			}
			visit[d][nr][nc] = true;
			
			if (arr[nr][nc] == 1) {
				d++;
				d = d % 4;
			}
			nr += delta[d][0];
			nc += delta[d][1];
//			for(int i[] : t)
//				System.out.println(Arrays.toString(i));
//			System.out.println();
		}
		nr -= delta[d][0];
		nc -= delta[d][1];
		
		
		return (nc + " " + (arr.length - 1-nr));
	}
}
