import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main_15953_������� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {

			StringTokenizer st = new StringTokenizer(br.readLine());
			int a = Integer.parseInt(st.nextToken());
			int b = Integer.parseInt(st.nextToken());

			int answer = 0;

			int price[] = new int[] { 500, 300, 200, 50, 30, 10 };
			int th[] = new int[] { 1, 3, 6, 10, 15, 21 };

			if (a != 0)
				for (int i = 0; i < price.length; i++) {
					if (th[i] >= a) {
						answer = price[i] * 10000;
						break;
					}
				}

			price = new int[] { 512, 256, 128, 64, 32 };
			th = new int[] { 1, 3, 7, 15, 31 };

			if (b != 0)
				for (int i = 0; i < price.length; i++) {
					if (th[i] >= b) {
						answer += price[i] * 10000;
						break;
					}
				}

			bw.write(Integer.toString(answer));
			bw.newLine();
			bw.flush();
		}
	}
}
