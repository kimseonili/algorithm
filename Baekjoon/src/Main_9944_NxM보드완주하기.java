import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main_9944_NxM보드완주하기 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int t = 0;
		String input = "";
		while ((input = br.readLine()) != null) {
			t++;
			StringTokenizer st = new StringTokenizer(input);
			int N = Integer.parseInt(st.nextToken());
			int M = Integer.parseInt(st.nextToken());
			char arr[][] = new char[N][M];

			int totalBlank = 0;
			minLevel = Integer.MAX_VALUE;
			for (int i = 0; i < N; i++) {
				String line = br.readLine();
				arr[i] = line.toCharArray();
				for (int j = 0; j < M; j++) {
					if (arr[i][j] == '.') {
						totalBlank++;
					}
				}
			}

			for (int i = 0; i < N; i++) {
				for (int j = 0; j < M; j++) {
					if (arr[i][j] == '.') {
						boolean visit[][] = new boolean[N][M];
						visit[i][j] = true;
						dfs(i, j, visit, arr, totalBlank, 1, 0);
					}
				}
			}
			if(minLevel == Integer.MAX_VALUE)
				minLevel = -1;
			System.out.println("Case " + t + ": " + minLevel);
		}
	}

	static int minLevel;
	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static void dfs(int r, int c, boolean visit[][], char arr[][], int totalBlank, int curBlank, int level) {
		if (minLevel < level) {
			return;
		}
		if (curBlank == totalBlank) {
			if (minLevel > level) {
				minLevel = level;
			}

			return;
		}

		for (int k = 0; k < delta.length; k++) {
			
			int nr = r + delta[k][0];
			int nc = c + delta[k][1];
			int visitBlank = 0;
			while (isRange(nr, nc, arr.length, arr[0].length) && !visit[nr][nc] && arr[nr][nc] == '.') {
				visitBlank++;
				visit[nr][nc] = true;
				nr += delta[k][0];
				nc += delta[k][1];
			}
			nr -= delta[k][0];
			nc -= delta[k][1];
			if (visitBlank > 0)
				dfs(nr, nc, visit, arr, totalBlank, curBlank + visitBlank, level + 1);
			while(nr != r || nc != c) {
				visit[nr][nc] = false;
				nr -= delta[k][0];
				nc -= delta[k][1];
			}
		}

	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}
	
//	static boolean[][] copyArr(boolean origin[][]){
//		boolean copyArr[][] = new boolean[origin.length][origin[0].length];
//		for(int i = 0 ; i < copyArr.length ; i++) {
//			for(int j = 0 ; j < copyArr[0].length ; j++) {
//				copyArr[i][j] = origin[i][j];
//			}
//		}
//		return copyArr;
//	}
}
