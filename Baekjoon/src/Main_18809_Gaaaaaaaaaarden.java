import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_18809_Gaaaaaaaaaarden {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int G = Integer.parseInt(st.nextToken());
		int R = Integer.parseInt(st.nextToken());
		int arr[][] = new int[N][M];
		LinkedList<int[]> tmpList = new LinkedList<>();
		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < M; j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
				if (arr[i][j] == 2) {
					tmpList.offer(new int[] { i, j });
				}
			}
		}

		int seeds[][] = new int[tmpList.size()][2];
		for (int i = 0; i < seeds.length; i++) {
			seeds[i] = tmpList.poll();
		}

		int comb[] = new int[seeds.length];
//		comb = new int[] {1,2,1,0,2,0,1,0};
//		BFS(seeds, comb, arr);
		comb(seeds, comb, arr, 0, G, R, 0, 0, 0);
		bw.write(answer + "");
		bw.flush();
	}

	static int answer;

	static void comb(int seeds[][], int comb[], int arr[][], int idx, int G, int R, int cntG, int cntR, int cntW) {
		if (idx == comb.length) {
			// do BFS
			int res = BFS(seeds, comb, arr);
//			System.out.println(Arrays.toString(comb) + " -> " + res);
			answer = answer < res ? res : answer;
			return;
		}
		if (G > cntG) {
			comb[idx] = 1;
			comb(seeds, comb, arr, idx + 1, G, R, cntG + 1, cntR, cntW);
		}
		if (R > cntR) {
			comb[idx] = 2;
			comb(seeds, comb, arr, idx + 1, G, R, cntG, cntR + 1, cntW);

		}
		if (seeds.length > cntW + R + G) {
			comb[idx] = 0;
			comb(seeds, comb, arr, idx + 1, G, R, cntG, cntR, cntW + 1);
		}
	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static int BFS(int seeds[][], int comb[], int arr[][]) {
		int answer = 0;
		int visit[][][] = new int[arr.length][arr[0].length][2];
		for (int i[][] : visit)
			for (int j[] : i)
				j[0] = Integer.MAX_VALUE;

		Queue<int[]> q = new LinkedList<>();
		int seedCnt = 0;
		for (int i = 0; i < comb.length; i++) {
			if (comb[i] > 0) {
				q.add(new int[] { seeds[seedCnt][0], seeds[seedCnt][1], comb[i], 0 });// 행,열,색,시간
				visit[seeds[seedCnt][0]][seeds[seedCnt][1]][0] = 0;
				visit[seeds[seedCnt][0]][seeds[seedCnt][1]][1] = comb[i];
			}
			seedCnt++;
		}
		int cnt = 0;
		while (!q.isEmpty()) {
			int cur[] = q.poll();
			if (visit[cur[0]][cur[1]][0] == -1) {
				continue;
			}
//			if (cnt == cur[3]) {
//				printMap(visit);
//				cnt++;
//			}
			for (int k = 0; k < 4; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];

				if (isRange(nr, nc, arr.length, arr[0].length) && visit[nr][nc][0] > cur[3] && arr[nr][nc] != 0) {
					if (cur[2] == visit[nr][nc][1])
						continue;
					if (cur[3] + 1 == visit[nr][nc][0]) {
						visit[nr][nc][0] = -1;
						visit[nr][nc][1] = 3;
						answer++;
						continue;
					}
					q.add(new int[] { nr, nc, cur[2], cur[3] + 1 });
					visit[nr][nc][0] = cur[3] + 1;
					visit[nr][nc][1] = cur[2];
				}
			}
		}
//		printMap(visit);
		return answer;
	}

	static void printMap(int visit[][][]) {
		for (int i[][] : visit) {
			for (int j[] : i) {
				System.out.print(j[1] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
}
