import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main_7682_ƽ���� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		String input;
		while (!(input = br.readLine()).equals("end")) {

			int xCount = 0;
			int oCount = 0;
			int dCount = 0;
			char[][] arr = new char[3][3];

			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					arr[i][j] = input.charAt(3 * i + j);
					if (arr[i][j] == 'O')
						oCount++;
					else if (arr[i][j] == 'X')
						xCount++;
					else
						dCount++;
				}
			}
			int res = calc(arr);
			boolean flag = false;
			if (res == 1) {
				if (xCount == oCount + 1)
					flag = true;
			} else if (res == 0) {
				if (xCount == oCount + 1 && dCount == 0)
					flag = true;
			} else if (res == -1) {
				if (xCount == oCount)
					flag = true;
			}

			if (!flag)
				bw.write("in");
			bw.write("valid");
			bw.newLine();
		}
		bw.flush();
	}

	// 1 : x ��, 0 : ���º�, -1 : o ��
	static int calc(char arr[][]) {
		// ����
		int xW = 0;
		int oW = 0;
		for (int i = 0; i < 3; i++) {
			if (arr[i][0] == arr[i][1] && arr[i][0] == arr[i][2]) {
				if (arr[i][0] == 'X') {
					xW++;
				} else if (arr[i][0] == 'O')
					oW++;
			}
		}
		
		if (xW > 1 || oW > 1 || (xW > 0 && oW > 0))
			return -2;
		if (xW == 1)
			return 1;
		if (oW == 1)
			return -1;
        xW = 0;
		oW = 0;
		// ����
		for (int i = 0; i < 3; i++) {
			if (arr[0][i] == arr[1][i] && arr[0][i] == arr[2][i]) {
				if (arr[0][i] == 'X') {
					xW++;
				} else if (arr[0][i] == 'O')
					oW++;
			}
		}

		if (xW > 1 || oW > 1 || (xW > 0 && oW > 0))
			return -2;
		if (xW == 1)
			return 1;
		if (oW == 1)
			return -1;
		// �밢

		if (arr[0][0] == arr[1][1] && arr[0][0] == arr[2][2]) {
			if (arr[0][0] == 'X')
				return 1;
			else if (arr[0][0] == 'O')
				return -1;
		}

		if (arr[0][2] == arr[1][1] && arr[1][1] == arr[2][0]) {
			if (arr[1][1] == 'X')
				return 1;
			else if (arr[1][1] == 'O')
				return -1;
		}

		return 0;
	}
}

//xox
//oxo
//.o.