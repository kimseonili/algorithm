import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.StringTokenizer;

public class Main_15573_ä�� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());

		int arr[][] = new int[R + 1][C + 2];
		boolean w[] = new boolean[1000001];
		int len = 0;
		for (int i = 1; i < arr.length; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 1; j < C + 1; j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
				if (!w[arr[i][j]]) {
					w[arr[i][j]] = true;
					len++;
				}
			}
		}

		int bin[] = new int[len];
		int idx = 0;
		for (int i = 1; i < w.length; i++) {
			if (w[i])
				bin[idx++] = i;
		}

		int p = len / 2;
		int s = 0;
		int e = len;
		int value = 0;

		int answer = Integer.MAX_VALUE;

		while (s <= e) {

			value = bfs(arr, bin[p]);
			
			
			
			if (value >= K) {
				if(answer > bin[p])
					answer = bin[p];
				e = p - 1;
			} else {
				s = p + 1;
			}
			p = (s + e) / 2;
		}

		bw.write(answer + "");

		bw.flush();
//		System.out.println(Arrays.toString(bin));
//		for(int i[] : arr)
//			System.out.println(Arrays.toString(i));

	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static int bfs(int arr[][], int p) {
		int answer = 0;

		Queue<int[]> q = new LinkedList<>();
		boolean visit[][] = new boolean[arr.length][arr[0].length];
		q.add(new int[] { 0, 0 });
		visit[0][0] = true;
		while (!q.isEmpty()) {
			int cur[] = q.poll();

			for (int k = 0; k < 4; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];

				if (isRange(nr, nc, arr.length, arr[0].length) && arr[nr][nc] <= p && !visit[nr][nc]) {
					visit[nr][nc] = true;
					q.add(new int[] { nr, nc });
					if (arr[nr][nc] > 0) {
						answer++;
					}
				}
			}

		}

		return answer;
	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}
}
