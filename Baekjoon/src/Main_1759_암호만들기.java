import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_1759_암호만들기 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int C = Integer.parseInt(st.nextToken());
		int N = Integer.parseInt(st.nextToken());

		char[] arr = new char[N];
		st = new StringTokenizer(br.readLine());
		for (int i = 0; i < N; i++) {
			arr[i] = st.nextToken().charAt(0);
		}
		Arrays.sort(arr);
		perm(arr, new char[C], 0, 0, 0, 0, C);
		bw.flush();

	}

	static BufferedWriter bw;

	static void perm(char[] arr, char[] res, int idx, int c, int a, int b, int C) throws IOException {
		if (c == C) {
			if (a > 0 && b > 1) {
				for (char ch : res)
					bw.write(ch);
				bw.newLine();
			}
			return;
		}

		if (idx == arr.length)
			return;

		res[c] = arr[idx];
		int na = a;
		int nb = b;
		if (arr[idx] == 'a' || arr[idx] == 'e' || arr[idx] == 'i' || arr[idx] == 'o' || arr[idx] == 'u')
			na++;
		else {
			nb++;
		}
		perm(arr, res, idx + 1, c + 1, na, nb, C);
		perm(arr, res, idx + 1, c, a, b, C);

	}
}
