import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_1021_회전하는큐 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int arr[] = new int[N];
		int pop[] = new int[M];
		st = new StringTokenizer(br.readLine());
		for (int i = 0; i < M; i++) {
			pop[i] = Integer.parseInt(st.nextToken());
		}
		for (int i = 0; i < N; i++) {
			arr[i] = i + 1;
		}
		bw.write(Integer.toString(shift(arr, pop)));
		bw.flush();
	}

	static int shift(int arr[], int pop[]) {
		int idx = 0;
		int target = 0;
		int answer = 0;
		while (target < pop.length) {
			int tmpL = idx;
			int tmpR = idx;
			int l = -0;
			int r = -0;
			boolean flagL = false;
			boolean flagR = false;
			// goLeft
			while (!flagR && !flagL) {

				while (tmpL == -1 || arr[tmpL] == 0) {
					if (tmpL == -1)
						tmpL = arr.length - 1;
					else
						tmpL--;
				}
				while (tmpR == arr.length || arr[tmpR] == 0) {
					if (tmpR == arr.length) {
						tmpR = 0;
					} else {
						tmpR++;
					}
				}
				if (arr[tmpR] == pop[target]) {
					flagR = true;
					break;
				}
				if (arr[tmpL] == pop[target]) {
					flagL = true;
					break;
				}
				tmpR++;
				tmpL--;
				l++;
				r++;
			}
			if (flagR) {
				answer += r;
				arr[tmpR] = 0;
				idx = tmpR;
//				System.out.println("Right! : " + r);
			} else {
				answer += l;
				arr[tmpL] = 0;
				idx = tmpL;
//				System.out.println("Left! : " + l);
			}
			target++;
			if (target < pop.length)
				while (idx == arr.length || arr[idx] == 0) {
					if (idx == arr.length)
						idx = 0;
					else
						idx++;
				}
//			System.out.println(idx + ":" + Arrays.toString(arr));
		} 

		return answer;
	}
}
