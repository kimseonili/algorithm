import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_1182_부분수열의합 {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int N = Integer.parseInt(st.nextToken());
		int S = Integer.parseInt(st.nextToken());
		
		int arr[] = new int[N];
		st = new StringTokenizer(br.readLine());
		for (int i = 0 ; i < N ; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		comb(arr, S, 0, 0, false);
		bw.write(answer + "");
		bw.flush();
	}
	
	static int answer = 0;
	static void comb(int arr[], int S, int sum, int idx, boolean flag) {
		
		if(flag && sum == S) {
			answer++;
		}
		
		if( idx == arr.length) {
			return;
			
		}
		
		comb(arr, S, sum+arr[idx], idx+1, true);
		comb(arr, S, sum, idx+1, false);
		
		
	}
}
