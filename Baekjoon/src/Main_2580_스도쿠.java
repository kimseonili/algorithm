import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_2580_������ {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int arr[][] = new int[9][9];

		StringTokenizer st;

		for (int i = 0; i < 9; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < 9; j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		checkAndFill(arr);
		int zero[][] = checkZeroLoc(arr);
		fill(arr, zero, 0);
	}

	static boolean flag;

	static void fill(int[][] arr, int[][] zero, int idx) {
//		for(int i[] : arr)
//			System.out.println(Arrays.toString(i));
		if(flag) {
			return;
		}
		if (idx == zero.length) {
			flag = true;
			for (int i = 0; i < 9; i++) {
				for (int j = 0; j < 9; j++) {
					System.out.print(arr[i][j]+" ");
				}
				System.out.println();
			}
			return;
		}
		int r = zero[idx][0];
		int c = zero[idx][1];
		int num[] = new int[10];
		Arrays.fill(num, 1);

		// checkCol

		for (int i = 0; i < 9; i++) {
			num[arr[i][c]] = 0;
		}

		// checkRow
		for (int i = 0; i < 9; i++) {
			num[arr[r][i]] = 0;
		}

		// checkSqure
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				num[arr[i + r / 3 * 3][j + c / 3 * 3]] = 0;
			}
		}

		for (int i = 1; i < 10 ; i++) {
			if (num[i] == 1) {
				arr[r][c] = i;
				fill(arr, zero, idx + 1);
			}
		}
		arr[r][c] = 0;
	}

	static void checkAndFill(int[][] arr) {
		boolean flag = false;
		// ����
		i: for (int i = 0; i < 9; i++) {
			int left = 45;
			int cntZero = 0;
			int locZero = -1;
			for (int j = 0; j < 9; j++) {
				left -= arr[i][j];
				if (arr[i][j] == 0) {
					cntZero++;
					locZero = j;
				}

				if (cntZero > 1) {
					continue i;
				}

			}
			if (cntZero == 1) {
				arr[i][locZero] = left;
				flag = true;
			}

		}

		i: for (int i = 0; i < 9; i++) {
			int left = 45;
			int cntZero = 0;
			int locZero = -1;
			for (int j = 0; j < 9; j++) {
				left -= arr[j][i];
				if (arr[j][i] == 0) {
					cntZero++;
					locZero = j;
				}
				if (cntZero > 1) {
					continue i;
				}
			}
			if (cntZero == 1) {
				arr[locZero][i] = left;
				flag = true;
			}
		}

		for (int i = 0; i < 3; i++) {
			i: for (int j = 0; j < 3; j++) {

				int left = 45;
				int cntZero = 0;
				int locZeroR = -1;
				int locZeroC = -1;

				for (int k = 0; k < 3; k++) {
					for (int l = 0; l < 3; l++) {
						int val = arr[3 * i + k][3 * j + l];
						left -= val;
						if (val == 0) {
							cntZero++;
							locZeroR = 3 * i + k;
							locZeroC = 3 * j + l;
						}
						if (cntZero > 1) {
							continue i;
						}
					}
				}

				if (cntZero == 1) {
					arr[locZeroR][locZeroC] = left;
					flag = true;
				}

			}
		}

		if (flag)
			checkAndFill(arr);
	}

	static int[][] checkZeroLoc(int arr[][]) {
		int cnt = 0;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (arr[i][j] == 0) {
					cnt++;
				}
			}
		}
		int[][] zero = new int[cnt][2];
		cnt = 0;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (arr[i][j] == 0) {
					zero[cnt][0] = i;
					zero[cnt][1] = j;
					cnt++;
				}
			}
		}
		return zero;
	}

}