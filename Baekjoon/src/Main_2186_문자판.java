import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_2186_������ {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());

		char[][] arr = new char[N][M];
		for (int i = 0; i < N; i++) {
			String input = br.readLine();
			for (int j = 0; j < M; j++) {
				arr[i][j] = input.charAt(j);
			}
		}

		String str = br.readLine();
		dp = new int[str.length()][N][M];
		
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				if (arr[i][j] == str.charAt(str.length() - 1))
					dp[str.length() - 1][i][j] = 1;
			}

		}

		calc(arr, str, str.length() - 2, K);
		
//		for (int i[][] : dp) {
//			for (int j[] : i) {
//				System.out.println(Arrays.toString(j));
//			}
//			System.out.println();
//		}
		
		bw.write(answer + "");
		bw.flush();
	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
	static int dp[][][];
	static int answer = 0;

	static void calc(char[][] arr, String str, int idx, int K) {
		if (idx == -1) {
			for(int i = 0 ; i < arr.length ; i++) {
				for(int j = 0 ; j < arr[0].length ; j++) {
					answer += dp[0][i][j];
				}
			}
			return;
		}
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				if (arr[i][j] == str.charAt(idx)) {
					for (int k = 0; k < delta.length; k++) {
						for (int c = 1; c <= K; c++) {
							int nr = i + delta[k][0] * c;
							int nc = j + delta[k][1] * c;
							if (isRange(nr, nc, arr.length, arr[0].length) && arr[nr][nc] == str.charAt(idx + 1)) {
								dp[idx][i][j] += dp[idx+1][nr][nc];
							}
						}
					}
				}
			}
		}

		calc(arr, str, idx - 1, K);

	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}
}
