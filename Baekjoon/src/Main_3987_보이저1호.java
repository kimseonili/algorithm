import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main_3987_������1ȣ {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());

		arr = new char[R][C];

		for (int i = 0; i < R; i++) {
			String str = br.readLine();
			for (int j = 0; j < C; j++) {
				arr[i][j] = str.charAt(j);
			}
		}

		st = new StringTokenizer(br.readLine());
		int r = Integer.parseInt(st.nextToken()) - 1;
		int c = Integer.parseInt(st.nextToken()) - 1;

		for (int i = 0; i < 4 && !voyager; i++) {
			char[] dir = { 'U', 'R', 'D', 'L' };
			visit = new boolean[4][arr.length][arr[0].length];
			shot(r, c, i, 0, dir[i]);
		}
		bw.write(dir);
		bw.newLine();
		if (voyager)
			bw.write("Voyager");
		else
			bw.write(answer + "");
		bw.flush();
	}

	static int delta[][] = { { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 } };
	static int answer;
	static char dir;
	static char[][] arr;
	static boolean visit[][][];
	static boolean voyager;

	static void shot(int r, int c, int d, int s, char originD) {

		if (!isRange(r, c, arr.length, arr[0].length)) {
			if (answer < s) {
				answer = s;
				dir = originD;
			}
			return;
		}

		if (visit[d][r][c]) {
			voyager = true;
			dir = originD;
			return;
		}

		if (arr[r][c] == 'C') {
			if (answer < s) {
				answer = s;
				dir = originD;
			}
			return;
		}
		visit[d][r][c] = true;
		int nd = d;
		if (arr[r][c] == '/') {
			if (d == 0)
				nd = 1;
			else if (d == 1)
				nd = 0;
			else if (d == 2)
				nd = 3;
			else if (d == 3)
				nd = 2;
		} else if (arr[r][c] == '\\') {
			if (d == 0)
				nd = 3;
			else if (d == 1)
				nd = 2;
			else if (d == 2)
				nd = 1;
			else if (d == 3)
				nd = 0;
		}
		int nr = r + delta[nd][0];
		int nc = c + delta[nd][1];

		shot(nr, nc, nd, s + 1, originD);

	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}
}
