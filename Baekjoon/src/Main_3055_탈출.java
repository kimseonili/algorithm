import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_3055_Ż�� {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());

		char arr[][] = new char[R][C];
		int start[] = new int[4];
		for (int i = 0; i < R; i++) {
			String str = br.readLine();
			for (int j = 0; j < C; j++) {
				arr[i][j] = str.charAt(j);
				if (arr[i][j] == 'S') {
					start[0] = i;
					start[1] = j;
					start[2] = 0;
					start[3] = 0;
				} else if (arr[i][j] == '*') {
					q.add(new int[] { i, j, 1 });
				}
			}
		}
		
		BFS(arr,start);
		if(answer == -1) {
			bw.write("KAKTUS");
		}else {
			bw.write(answer + "");
		}
		bw.flush();
	}

	static int answer = 0;
	static Queue<int[]> q = new LinkedList<>();
	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static void BFS(char arr[][], int start[]) {
		q.add(start);
		boolean visit[][] = new boolean[arr.length][arr[0].length];
		visit[start[0]][start[1]] = true;
		while (!q.isEmpty()) {

			int cur[] = q.poll();
			for (int k = 0; k < 4; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];
				if (cur[2] == 0) {
					if (isRange(nr, nc, arr.length, arr[0].length) && !visit[nr][nc]) {
						if (arr[nr][nc] == 'D') {
							answer = cur[3] + 1;
							return;
						} else if (arr[nr][nc] == '.') {
							visit[nr][nc] = true;
							q.add(new int[] {nr,nc,0,cur[3] + 1});
						}
					}
				}
				else if(cur[2] == 1) {
					if (isRange(nr, nc, arr.length, arr[0].length) && arr[nr][nc] == '.') {
						arr[nr][nc] = '*';
						q.add(new int[] {nr,nc,1});
					}
				}
			}
//			System.out.println();
//			for(int i = 0 ; i < arr.length ; i++) {
//				System.out.println(Arrays.toString(arr[i]));
//			}

		}
		answer = -1;
	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}
}
