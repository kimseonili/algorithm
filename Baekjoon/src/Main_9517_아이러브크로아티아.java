import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main_9517_아이러브크로아티아 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st;

		int K = Integer.parseInt(br.readLine());

		int N = Integer.parseInt(br.readLine());

		int count = 210;
		boolean answer[] = new boolean[N];
		int time[] = new int[N];
		
		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			int T = Integer.parseInt(st.nextToken());
			char Z = st.nextToken().charAt(0);
			
			if(Z == 'T')
				answer[i] = true;
			time[i] = T;

		}
		int ans = K-1;
		
		for(int i = 0 ; i < N && count > 0 ; i++) {
			
			count -= time[i];
			if(answer[i] && count > 0) ans = (ans + 1 ) % 8;
		}
		ans++;
		bw.write(ans + "");
		bw.flush();
	}
}
