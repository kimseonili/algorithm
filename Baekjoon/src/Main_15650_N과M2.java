import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_15650_N��M2 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		int arr[] = new int[N];
		for (int i = 0; i < N; i++)
			arr[i] = i + 1;
		perm(arr, new int[C], 0, 0,C);
	}

	static void perm(int arr[], int res[], int idx,int start, int max) {
		if (idx == max) {
			for (int i : res)
				System.out.print(i + " ");
			System.out.println();
			return;
		}
		for (int i = start; i < arr.length; i++) {

			res[idx] = arr[i];
			perm(arr, res, idx+1, i+1, max);
			
			
		}
	}

}
