import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_4577_���ڹ� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int idx = 0;
		while (true) {
			StringTokenizer st = new StringTokenizer(br.readLine());

			int R = Integer.parseInt(st.nextToken());
			int C = Integer.parseInt(st.nextToken());
			if (R == 0)
				break;
			int start[] = null;
			int unCompletedBox = 0;
			char[][] arr = new char[R][C];
			for (int i = 0; i < R; i++) {
				String input = br.readLine();
				for (int j = 0; j < C; j++) {
					arr[i][j] = input.charAt(j);
					if (arr[i][j] == 'w' || arr[i][j] == 'W') {
						start = new int[] { i, j };
					} else if (arr[i][j] == 'b') {
						unCompletedBox++;
					}
				}
			}
			String command = br.readLine();

			BFS(arr, start, unCompletedBox, command, idx++);
		}
	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static void BFS(char[][] arr, int[] start, int uCBox, String command, int idx) {

		Queue<int[]> q = new LinkedList<>();
		q.offer(start);
		int commIdx = -1;
		while (commIdx < command.length() - 1 && !q.isEmpty() && uCBox > 0) {
			commIdx++;

			int cur[] = q.poll();
			int k = -1;
			if (command.charAt(commIdx) == 'U') {
				k = 0;
			} else if (command.charAt(commIdx) == 'D') {
				k = 1;
			} else if (command.charAt(commIdx) == 'L') {
				k = 2;
			} else {
				k = 3;
			}
			int nr = cur[0] + delta[k][0];
			int nc = cur[1] + delta[k][1];

			if (arr[nr][nc] == '#') {
				q.add(cur);
				continue;
			}
			if (arr[nr][nc] == 'b' || arr[nr][nc] == 'B') {
				if (arr[nr + delta[k][0]][nc + delta[k][1]] == '#'
						|| Character.toLowerCase(arr[nr + delta[k][0]][nc + delta[k][1]]) == 'b') {
					q.add(cur);
					continue;
				}
				if (arr[nr + delta[k][0]][nc + delta[k][1]] == '+') {
					arr[nr + delta[k][0]][nc + delta[k][1]] = 'B';
					uCBox--;
				} else {
					arr[nr + delta[k][0]][nc + delta[k][1]] = 'b';
				}
				if (arr[nr][nc] == 'B') {
					arr[nr][nc] = 'W';
					uCBox++;
				} else {
					arr[nr][nc] = 'w';
				}
				if (arr[cur[0]][cur[1]] == 'W') {
					arr[cur[0]][cur[1]] = '+';
				} else {
					arr[cur[0]][cur[1]] = '.';

				}
				q.add(new int[] { nr, nc });
			}
			if (arr[nr][nc] == '.') {
				q.add(new int[] { nr, nc });
				arr[nr][nc] = 'w';
				if (arr[cur[0]][cur[1]] == 'W')
					arr[cur[0]][cur[1]] = '+';
				else
					arr[cur[0]][cur[1]] = '.';
			}
			if (arr[nr][nc] == '+') {
				arr[nr][nc] = 'W';
				q.add(new int[] { nr, nc });
				if (arr[cur[0]][cur[1]] == 'W')
					arr[cur[0]][cur[1]] = '+';
				else
					arr[cur[0]][cur[1]] = '.';
			}
		}
		System.out.print("Game " + (idx + 1) + ": ");
		if (uCBox > 0) {
			System.out.print("in");
		}
		System.out.println("complete");
		print(arr);
	}

	static void print(char arr[][]) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				System.out.print(arr[i][j]);
			}
			System.out.println();
		}
	}
}
