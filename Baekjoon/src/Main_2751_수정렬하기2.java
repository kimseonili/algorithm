import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main_2751_수정렬하기2 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int N = Integer.parseInt(br.readLine());
		int arr[] = new int[2000001];
		for (int i = 0; i < N; i++) {
			arr[Integer.parseInt(br.readLine()) + 1000000] = 1;
		}

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > 0) {
				bw.write(Integer.toString(i - 1000000));
				bw.newLine();
			}
		}
		bw.flush();

	}
}
