import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main_1051_숫자정사각형 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());

		int arr[][] = new int[R][C];

		for (int i = 0; i < R; i++) {
			String input = br.readLine();
			for (int j = 0; j < C; j++) {

				arr[i][j] = input.charAt(j) - '0';

			}
		}

		for (int i = 0; i < R; i++) {
			for (int j = 0; j < C; j++) {
				find(arr, i, j);
				
			}
		}
		bw.write(answer * answer + "");
		bw.flush();
		

	}

	static int answer = 1;
	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}
	static void find(int arr[][], int r, int c) {

		int len = Math.min(arr.length - r, arr[0].length - c);
		
		for(int i = len - 1 ; i > 0 ; i--) {
			if(i + 1<= answer) return;
			
			int a = arr[r + i][c];
			int b = arr[r][c + i];
			int ab = arr[r + i][c + i];
			
			if(arr[r][c] == a && arr[r][c] == b && arr[r][c] == ab) {
				answer = i+1;
				return;
			}
		}
		
		
	}
}
