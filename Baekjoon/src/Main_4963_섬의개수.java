import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_4963_���ǰ��� {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st;

		while (true) {
			st = new StringTokenizer(br.readLine());
			int W = Integer.parseInt(st.nextToken());
			int H = Integer.parseInt(st.nextToken());
			if (W + H == 0) {
				break;
			}
			answer = 0;
			int arr[][] = new int[H][W];
			boolean visit[][] = new boolean[H][W];
			Queue<int[]> q = new LinkedList<>();
			for (int i = 0; i < H; i++) {
				st = new StringTokenizer(br.readLine());
				for (int j = 0; j < W; j++) {
					arr[i][j] = Integer.parseInt(st.nextToken());
					if (arr[i][j] == 1) {
						q.add(new int[] { i, j });
					}
				}
			}
			while(!q.isEmpty()) {
				int cur[] = q.poll();
				BFS(arr, visit, cur[0], cur[1]);
			}
			bw.write(Integer.toString(answer));
			bw.newLine();
		}
		bw.flush();
	}

	static int answer;
	static int delta[][] = { { -1, -1 }, { -1, 0 }, { -1, 1 }, { 0, -1 }, { 0, 1 }, { 1, -1 }, { 1, 0 }, { 1, 1 } };
	static void BFS(int arr[][], boolean visit[][], int r, int c) {
		Queue <int[]>q = new LinkedList<>();
		
		if(visit[r][c]) {
			return;
		}
		
		q.add(new int[] {r,c});
		visit[r][c] = true;
		answer += 1;
		while(!q.isEmpty()) {
			int cur[] = q.poll();
			
			for(int k = 0 ; k < delta.length ; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];
				if(isRange(nr,nc,arr.length, arr[0].length) && arr[nr][nc] == 1 && !visit[nr][nc]) {
					q.add(new int[] {nr,nc});
					visit[nr][nc] = true;
				}
			}
			
		}
	}
	static boolean isRange(int nr, int nc, int H, int W) {
		return nr >= 0 && nc >= 0 && nr < H && nc < W;
	}
}