import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_13549_���ٲ���3 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());

		bw.write(bfs(N, K) + "");

		bw.flush();
	}

	static PriorityQueue<int[]> q = new PriorityQueue<>(new Comparator<int[]>() {

		@Override
		public int compare(int[] o1, int[] o2) {
			// TODO Auto-generated method stub
			return o1[1] - o2[1];
		}
	});

	static int bfs(int N, int K) {
		q.add(new int[] { N, 0 });
		int visit[] = new int[100001];
		Arrays.fill(visit, Integer.MAX_VALUE);
		visit[N] = 0;
		while (true) {
			int cur[] = q.poll();
			if (cur[0] == K)
				return cur[1];
			if (cur[0] * 2 < visit.length) {
				
				if (cur[0] != 0 && visit[cur[0] * 2] > cur[1]) {
					q.add(new int[] { cur[0] * 2, cur[1] });
					visit[cur[0] * 2] = cur[1];
				}
			}
			if (cur[0] > 0) {
				if (visit[cur[0] - 1] > cur[1] + 1) {
					q.add(new int[] { cur[0] - 1, cur[1] + 1 });
					visit[cur[0] - 1] = cur[1] + 1;
				}
			}
			if (cur[0] < visit.length - 1) {
				if (visit[cur[0] + 1] > cur[1] + 1) {
					q.add(new int[] { cur[0] + 1, cur[1] + 1 });
					visit[cur[0] + 1] = cur[1] + 1;
				}
			}

		}

	}
}
