import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_13459_����Ż�� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		char arr[][] = new char[N][M];
		int blue[] = new int[2];
		int red[] = new int[2];
		int goal[] = new int[2];
		for (int i = 0; i < N; i++) {
			String input = br.readLine();
			for (int j = 0; j < M; j++) {
				arr[i][j] = input.charAt(j);
				if (arr[i][j] == 'B') {
					blue[0] = i;
					blue[1] = j;
				}
				if (arr[i][j] == 'R') {
					red[0] = i;
					red[1] = j;
				}
				if (arr[i][j] == 'O') {
					goal[0] = i;
					goal[1] = j;
				}
			}
		}
		move(arr, blue, red, 0, 0);
		System.out.println(isEnd);
	}

	static boolean isEnd;
	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static void move(char[][] arr, int blue[], int red[], int dir, int cnt) {
		if(cnt == 0) {
			for(int k = 0 ; k < 4; k++) {
				move(arr, blue, red, k, cnt+1);
			}
			return;
		}
		
		if (cnt > 10 || isEnd) {
//			printMap(arr);
			return;
		}
		Comparator<int[]> c = new Comparator<int[]>() {

			@Override
			public int compare(int[] o1, int[] o2) {
				switch (dir) {
				case 0:
					return o1[0] - o2[0];
				case 1:
					return o2[0] - o1[0];
				case 2:
					return o1[1] - o2[1];
				case 3:
					return o2[1] - o1[1];
				}
				return -1;
			}
		};
		char copyArr[][] = new char[arr.length][arr[0].length];
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				copyArr[i][j] = arr[i][j];
			}
		}
		PriorityQueue<int[]> pq = new PriorityQueue<>(c);
		pq.add(new int[] { red[0], red[1] });
		pq.add(new int[] { blue[0], blue[1] });
		int[] copyBlue = new int[2];
		int[] copyRed = new int[2];
		boolean isBlue = false;
		boolean isRed = false;
		while (!pq.isEmpty()) {
			int cur[] = pq.poll();
			char color = copyArr[cur[0]][cur[1]];
			int nr = cur[0] + delta[dir][0];
			int nc = cur[1] + delta[dir][1];
			if (isRange(nr, nc, arr.length, arr[0].length) && (copyArr[nr][nc] == '.' || copyArr[nr][nc] == 'O')) {
				if (arr[nr][nc] == 'O') {
					if(color == 'R') {
						copyArr[cur[0]][cur[1]] = '.';
						isRed = true;
					}else if(color == 'B') {
						isBlue = true;
						return;
					}
				}
				copyArr[nr][nc] = copyArr[cur[0]][cur[1]];
				copyArr[cur[0]][cur[1]] = '.';
				pq.add(new int[] {nr,nc});
			} else {
				if (color == 'B') {
					copyBlue = new int[] { cur[0], cur[1] };
				} else {
					copyRed = new int[] { cur[0], cur[1] };
				}
			}

		}
		if(isRed && !isBlue) {
			isEnd = true;
			return;
		}
		for(int k = 0 ; k < 4 ; k++) {
			move(copyArr, copyBlue, copyRed, k, cnt+1);
		}

	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}
	
	static void printMap(char[][] arr) {
		for(char c[]: arr)
			System.out.println(Arrays.toString(c));
		System.out.println();
	}

}
