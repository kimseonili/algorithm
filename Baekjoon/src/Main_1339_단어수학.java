import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;

public class Main_1339_�ܾ���� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int N = Integer.parseInt(br.readLine());
		String inputs[] = new String[N];

		int alp[] = new int[26];
		Arrays.fill(alp, -1);
		int cnt = 0;
		for (int i = 0; i < N; i++) {
			inputs[i] = br.readLine();
			for (int j = 0; j < inputs[i].length(); j++) {

				if (alp[inputs[i].charAt(j) - 'A'] == -1) {
					cnt++;
					alp[inputs[i].charAt(j) - 'A'] = 1;
				}
			}
		}
		int val = 9;
		int num[] = new int[cnt];
		for (int i = 0; i < cnt; i++) {
			num[i] = val--;
		}
		perm(num, 0, alp, inputs);
		bw.write(answer + "");
		bw.flush();

	}

	static int convToNum(int[] alp, String input) {
		int res = 0;
		for (int i = 0; i < input.length(); i++) {
			res = res * 10 + alp[input.charAt(i) - 'A'];
		}
		return res;
	}

	static int answer;

	static void perm(int num[], int idx, int alp[], String inputs[]) {
		if (idx == num.length) {
			int cnt = 0;
			for (int i = 0; i < alp.length && cnt < num.length; i++) {
				if (alp[i] > -1) {
					alp[i] = num[cnt++];
				}
			}
			int res = 0;
			for (int i = 0; i < inputs.length; i++) {
				res += convToNum(alp, inputs[i]);
			}
			if (res > answer) {
				answer = res;
			}
			return;
		}

		for (int i = idx; i < num.length; i++) {
			int tmp = num[idx];
			num[idx] = num[i];
			num[i] = tmp;
			perm(num, idx + 1, alp, inputs);
			tmp = num[idx];
			num[idx] = num[i];
			num[i] = tmp;
		}
	}

}
