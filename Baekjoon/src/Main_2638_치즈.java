import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_2638_ġ�� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		int arr[][] = new int[R + 2][C + 2];
		for (int i = 0; i < R; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < C; j++) {
				arr[i + 1][j + 1] = Integer.parseInt(st.nextToken());
			}
		}

		bw.write(bfs(arr) + "");
		bw.flush();

	}

	static Queue<int[]> q = new LinkedList<>();
	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}

	static int bfs(int arr[][]) {
		int answer = 0;
		boolean flag = true;
		while (flag) {
			int visit[][] = new int[arr.length][arr[0].length];
			visit[0][0] = 2;
			q.add(new int[] { 0, 0 });
			flag = false;
			answer++;
			while (!q.isEmpty()) {
				int cur[] = q.poll();

				for (int k = 0; k < 4; k++) {
					int nr = cur[0] + delta[k][0];
					int nc = cur[1] + delta[k][1];
					if (isRange(nr, nc, arr.length, arr[0].length) && visit[nr][nc] < 2) {
						if (arr[nr][nc] == 0) {
							q.add(new int[] { nr, nc });
							visit[nr][nc] = 2;
						} else if (arr[nr][nc] == 1) {
							visit[nr][nc]++;
							if (visit[nr][nc] == 2) {
								flag = true;
								arr[nr][nc] = 0;
							}
						}
					}
				}
			}
//			System.out.println(answer);
//			for (int i[] : arr)
//				System.out.println(Arrays.toString(i));
//			System.out.println();
		}
		return answer - 1;
	}

}
