import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main_10845_ť {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int N = Integer.parseInt(br.readLine());
		StringTokenizer st;
		MyQueue q = new MyQueue();
		for (int i = 0; i < N; i++) {

			st = new StringTokenizer(br.readLine());

			switch (st.nextToken()) {
			case "push":
				int X = Integer.parseInt(st.nextToken());
				q.push(X);
				break;
			case "pop":
				bw.write(q.pop() + "\n");
				break;
			case "size":
				bw.write(q.size() + "\n");
				break;
			case "empty":
				bw.write(q.empty() + "\n");
				break;
			case "front":
				bw.write(q.front() + "\n");
				break;
			case "back":
				bw.write(q.back() + "\n");
				break;
			}

		}
		bw.flush();
	}

}
class N {
	int data;
	N next;

	public N(int X) {
		data = X;
	}
}
class MyQueue {

	int size;
	N front;
	N back;

	public MyQueue() {

	}

	void push(int X) {
		N node = new N(X);
		if (front == null) {
			front = node;
			back = node;
		} else {
			back.next = node;
			back = node;
		}
		size++;
	}

	int pop() {
		if (size == 0)
			return -1;
		size--;
		int pop = front.data;
		front = front.next;
		return pop;
	}

	int size() {
		return size;
	}

	int empty() {
		if (size > 0)
			return 0;

		return 1;
	}

	int front() {
		if(front == null) return -1;
		return front.data;
	}

	int back() {
		if(front == null) return -1;
		return back.data;
	}

}


