import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_19238_��ŸƮ�ý� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int N, M, F;

		StringTokenizer st = new StringTokenizer(br.readLine());

		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		F = Integer.parseInt(st.nextToken());
		int[][] arr = new int[N][N];

		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < N; j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		st = new StringTokenizer(br.readLine());
		int[] sLoc = new int[] { Integer.parseInt(st.nextToken()) - 1, Integer.parseInt(st.nextToken()) - 1 };
		int[][] cust = new int[M][4];
		for (int i = 0; i < M; i++) {
			st = new StringTokenizer(br.readLine());
			int sr = Integer.parseInt(st.nextToken());
			int sc = Integer.parseInt(st.nextToken());
			int dr = Integer.parseInt(st.nextToken());
			int dc = Integer.parseInt(st.nextToken());
			cust[i] = new int[] { sr - 1, sc - 1, dr - 1, dc - 1 };
		}
		int answer = BFS(arr, M, F, sLoc, cust);
		bw.write(answer + "");
		bw.flush();
	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static boolean isRange(int nr, int nc, int N) {
		return nr >= 0 && nc >= 0 && nr < N && nc < N;
	}

	static int BFS(int arr[][], int M, int F, int[] sLoc, int[][] cust) {

		Queue<int[]> q = new LinkedList<>();
		Queue<int[]> q2 = new LinkedList<>();
		for (int i = 0; i < cust.length; i++) {
			if (sLoc[0] == cust[i][0] && cust[i][1] == sLoc[1]) {

				q2.add(new int[] { sLoc[0], sLoc[1], 0, F, 0, i });
				break;
			}
		}
		if (q2.isEmpty())
			q.add(new int[] { sLoc[0], sLoc[1], 0, F, 0, -1 });
		// r, c,�����°�, ��������, ��뿬��, �¿� �°�
		int N = arr.length;
		boolean visit[][] = new boolean[N][N];
		int completeCnt = 0;
		visit[sLoc[0]][sLoc[1]] = true;
		int answer = -1;
		while (completeCnt < M) {
			int minDist = Integer.MAX_VALUE;
			int minR = N;
			int minC = N;
			while (!q.isEmpty()) {
				int cur[] = q.poll();
				if (cur[4] + 1 > minDist || cur[3] <= 0) {
					continue;
				}
				for (int k = 0; k < 4; k++) {
					int nr = cur[0] + delta[k][0];
					int nc = cur[1] + delta[k][1];
					if (isRange(nr, nc, N) && !visit[nr][nc] && arr[nr][nc] != 1) {
						visit[nr][nc] = true;
						q.add(new int[] { nr, nc, cur[2], cur[3] - 1, cur[4] + 1, -1 });
						for (int i = 0; i < cust.length; i++) {
							if (nr == cust[i][0] && nc == cust[i][1]) {
								if (minDist == cur[4] + 1) {
									if (nr < minR || (nr <= minR && nc < minC)) {
										minR = nr;
										minC = nc;
										q2.clear();
										q2.add(new int[] { nr, nc, cur[2], cur[3] - 1, 0, i });
									}

								} else if (minDist > cur[4] + 1) {
									minDist = cur[4] + 1;
									minR = nr;
									minC = nc;
									q2.clear();
									q2.add(new int[] { nr, nc, cur[2], cur[3] - 1, 0, i });

								}
							}
						}
					}
				}
			}
			if (q2.isEmpty()) {
				break;
			}
			int startLoc[] = q2.peek();
			visit = new boolean[N][N];
			visit[startLoc[0]][startLoc[1]] = true;
			q2: while (!q2.isEmpty()) {
				int cur[] = q2.poll();
				if (cur[3] <= 0)
					continue;
				for (int k = 0; k < 4; k++) {
					int nr = cur[0] + delta[k][0];
					int nc = cur[1] + delta[k][1];
					if (isRange(nr, nc, N) && !visit[nr][nc] && arr[nr][nc] != 1) {
						if (cust[cur[5]][2] == nr && cust[cur[5]][3] == nc) {
							completeCnt++;
							q2.clear();
							cust[cur[5]][0] = -1;
							visit = new boolean[N][N];
							visit[nr][nc] = true;
							for (int i = 0; i < cust.length; i++) {
								if (cust[i][0] == nr && cust[i][1] == nc) {
									q2.add(new int[] { nr, nc, cur[2] + 1, cur[3] - 1 + (cur[4] + 1) * 2, 0, i });
									continue q2;
								}
							}
							q.add(new int[] { nr, nc, cur[2] + 1, cur[3] - 1 + (cur[4] + 1) * 2, 0, -1 });
							answer = cur[3] - 1 + (cur[4] + 1) * 2;
							break q2;
						} else {
							visit[nr][nc] = true;
							q2.add(new int[] { nr, nc, cur[2], cur[3] - 1, cur[4] + 1, cur[5] });
						}
					}
				}
			}
//			printMap(arr);
//			System.out.println(Arrays.toString(q.peek()));
//			System.out.println();
//			for(int i[] : cust)
//				System.out.println(Arrays.toString(i));
		}
		if (completeCnt == M) {
			return answer;
		} else {
			return -1;
		}
	}

	static void printMap(int[][] arr) {
		for (int i[] : arr) {
			System.out.println(Arrays.toString(i));
		}
		System.out.println();
	}
}
