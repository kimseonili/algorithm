import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main_15652_N��M4 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		int arr[] = new int[N];
		for (int i = 0; i < N; i++) {
			arr[i] = i + 1;
		}

		perm(arr, new int[C], 0, 0, C);
		bw.flush();
	}

	static BufferedWriter bw;

	static void perm(int[] arr, int res[], int idx, int c, int max) throws IOException{

		if (c == max) {
			for (int i : res)
				bw.write(i + " ");
			bw.newLine();

			return;
		}

		for (int i = idx; i < arr.length; i++) {

			res[c] = arr[i];
			perm(arr, res, i, c + 1, max);

		}

	}
}
