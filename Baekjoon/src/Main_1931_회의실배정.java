import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;

public class Main_1931_회의실배정 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int N = Integer.parseInt(br.readLine());

		int arr[][] = new int[N][2];

		for (int i = 0; i < N; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			arr[i][0] = Integer.parseInt(st.nextToken());
			arr[i][1] = Integer.parseInt(st.nextToken());
		}

		Arrays.sort(arr, new Comparator<int[]>() {

			@Override
			public int compare(int[] o1, int[] o2) {
				// TODO Auto-generated method stub
				
				
				
				
				if (o1[1] < o2[1]) {
					return -1;
				} else if (o1[1] == o2[1]) {
					if (o1[0] < o2[0]) {
						if(o2[1] == o2[1])
							return -1;
						return 1;
					} else if (o1[0] == o2[0]) {
						return 0;
					} else {
						if(o1[1] == o1[1])
							return 1;
						return -1;
					}
				} else {
					
					return 1;
				}
			}
		});
		System.out.println(Arrays.deepToString(arr));
		int time = arr[0][1];
		int answer = 1;
		for (int i = 1  ; i < N ; i++) {
			if(time <= arr[i][0]) {
				answer ++;
				time = arr[i][1];
			}
		}
		
		bw.write(answer +"");
		bw.flush();
	}
}
