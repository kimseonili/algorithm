import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_1012_�������� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st;

		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {
			st = new StringTokenizer(br.readLine());
			int C = Integer.parseInt(st.nextToken());
			int R = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());
			int arr[][] = new int[R][C];
			boolean visit[][] = new boolean[R][C];
			int bachu[][] = new int[K][2];
			for (int i = 0; i < K; i++) {
				st = new StringTokenizer(br.readLine());

				bachu[i][1] = Integer.parseInt(st.nextToken());
				bachu[i][0] = Integer.parseInt(st.nextToken());
				arr[bachu[i][0]][bachu[i][1]] = 1;
			}
			BFS(arr, visit, bachu);
			bw.write(answer + "");
			bw.newLine();

		}
		bw.flush();
	}

	static Queue<int[]> q;
	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
	static int answer;
	static void BFS(int[][] arr, boolean visit[][], int[][] bachu) {
		answer = 0;
		for(int i = 0 ; i < bachu.length ; i++) {
			if(visit[bachu[i][0]][bachu[i][1]]) continue;
			visit[bachu[i][0]][bachu[i][1]] = true;
			q= new LinkedList<>();
			q.add(bachu[i]);
			answer++;
			while(!q.isEmpty()) {
				int cur[] = q.poll();
				for(int k = 0 ; k < 4; k++) {
					int nr = cur[0] + delta[k][0];
					int nc = cur[1] + delta[k][1];
					
					if(isRange(nr,nc, arr.length, arr[0].length) && arr[nr][nc] == 1 && !visit[nr][nc]) {
						visit[nr][nc] = true;
						q.add(new int[] {nr,nc});
					}
				}
				
			}
		}
		
	}
	
	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc <C;
	}
}
