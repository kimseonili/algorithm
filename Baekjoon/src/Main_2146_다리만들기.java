import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_2146_다리만들기 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st;

		int N = Integer.parseInt(br.readLine());

		int arr[][] = new int[N][N];

		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < arr.length; j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
				if (arr[i][j] == 1)
					arr[i][j] = -1;

			}
		}
		boolean visit[][] = new boolean[N][N];
		int islandCount = 1;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < arr.length; j++) {
				if (!visit[i][j] && arr[i][j] == -1) {
					arr[i][j] = islandCount;
					visit[i][j] = true;
					bridgeQueue[islandCount] = new LinkedList<int[]>();
					q.add(new int[] { i, j });
					while (!q.isEmpty()) {
						int cur[] = q.poll();
						for (int k = 0; k < 4; k++) {
							int nr = cur[0] + delta[k][0];
							int nc = cur[1] + delta[k][1];
							if (isRange(nr, nc, N) && arr[nr][nc] == 0) {
								bridgeQueue[islandCount].add(new int[] { cur[0], cur[1], islandCount, 0 });
								break;
							}
						}
						for (int k = 0; k < 4; k++) {
							int nr = cur[0] + delta[k][0];
							int nc = cur[1] + delta[k][1];

							if (isRange(nr, nc, N) && !visit[nr][nc]) {
								if (arr[nr][nc] == -1) {
									arr[nr][nc] = islandCount;
									visit[nr][nc] = true;
									q.add(new int[] { nr, nc });
								}
							}
						}
					}
					islandCount++;
				}

			}
		}



		for (int i = 1; i < islandCount; i++) {
			visit = new boolean[N][N];
			while (!bridgeQueue[i].isEmpty()) {
				int cur[] = bridgeQueue[i].poll();
				if(cur[3] >= answer)
					break;
				for (int k = 0; k < 4; k++) {
					int nr = cur[0] + delta[k][0];
					int nc = cur[1] + delta[k][1];
					if (isRange(nr, nc, N) && !visit[nr][nc]) {
						if (arr[nr][nc] == 0) {
							visit[nr][nc] = true;
							bridgeQueue[i].add(new int[] { nr, nc, cur[2], cur[3] + 1 });
						} else if (cur[2] != arr[nr][nc]) {
							if (answer > cur[3])
								answer = cur[3];
						}
					}
				}
			}

		}
		bw.write(answer + "");
		bw.flush();

	}

	static int answer = Integer.MAX_VALUE;
	static Queue<int[]> q = new LinkedList<>();
	static Queue<int[]> bridgeQueue[] = new LinkedList[10000];
	static int[][] delta = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static boolean isRange(int nr, int nc, int N) {
		return nr >= 0 && nc >= 0 && nr < N && nc < N;
	}
}
