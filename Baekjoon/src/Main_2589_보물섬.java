import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_2589_������ {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());

		int arr[][] = new int[R][C];

		for (int i = 0; i < R; i++) {
			String input = br.readLine();
			for (int j = 0; j < C; j++) {
				if (input.charAt(j) == 'W')
					arr[i][j] = 1;
				else
					arr[i][j] = 0;
			}
		}
		
		bfs(arr);
		bw.write(answer + "");
		bw.flush();

	}

	static int[][] delta = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
	static int answer = 0;

	static boolean isRange(int nr, int nc, int R , int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}

	static void bfs(int arr[][]) {

		Queue<int[]> q;
		boolean visit[][];

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				if(arr[i][j] == 1) continue;
				q = new LinkedList<>();
				q.add(new int[] { i, j, 0 });
				visit = new boolean[arr.length][arr[0].length];
				visit[i][j] = true;
				
				while (!q.isEmpty()) {

					int cur[] = q.poll();
					
					for(int k = 0 ; k < 4 ; k++) {
						int nr = cur[0] + delta[k][0];
						int nc = cur[1] + delta[k][1];
						if(isRange(nr, nc, arr.length, arr[0].length) && !visit[nr][nc] && arr[nr][nc] == 0) {
							
							visit[nr][nc] = true;
							if(answer < cur[2] + 1) {
								answer = cur[2] + 1;
							}
							q.add(new int[] {nr,nc,cur[2]+1});
							
						}
					}
					
				}

			}

		}
	}
}
