import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_3109_���� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());

		char[][] arr = new char[R][C];
		visit = new boolean[R][C];

		for (int i = 0; i < R; i++) {
			String input = br.readLine();
			for (int j = 0; j < C; j++) {
				arr[i][j] = input.charAt(j);
			}
		}

		for (int i = 0; i < R; i++) {
			flag = false;
			dfs(arr, i, 0);
		}


		bw.write(answer + "");
		bw.flush();

	}

	static int delta[][] = { { -1, 1 }, { 0, 1 }, { 1, 1 } };
	static int answer = 0;
	static boolean flag;
	static boolean visit[][];

	static void dfs(char arr[][], int r, int c) {
		visit[r][c] = true;
		if (c == arr[0].length - 1) {
			answer++;
			flag = true;
			return;
		}

		for (int k = 0; k < 3; k++) {
			int nr = r + delta[k][0];
			int nc = c + delta[k][1];

			if (isRange(nr, nc, arr.length, arr[0].length) && arr[nr][nc] == '.' && !visit[nr][nc]) {
				
				dfs(arr, nr, nc);
				if(flag) return;
			}

		}

	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}

}
