import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class Main_1520_�������� {
	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());

		int arr[][] = new int[R][C];

		for (int i = 0; i < R; i++) {

			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < C; j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
			}

		}
		bw.write(bfs(arr) + "");
		bw.flush();
//		
//		for(int i = 0  ; i < R ; i++)
//			for(int j = 0 ; j < C ; j++)
//				System.out.println(Arrays.toString(q.poll()));

	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
	static PriorityQueue<int[]> q = new PriorityQueue(new Comparator<int[]>() {

		@Override
		public int compare(int[] o1, int[] o2) {
			// TODO Auto-generated method stub
			return o2[2] - o1[2];
		}
	});

	static int bfs(int arr[][]) {

		q.add(new int[] { 0, 0, arr[0][0] });
		int[][] visit = new int[arr.length][arr[0].length];
		visit[0][0] = 1;
		while (!q.isEmpty()) {

			int cur[] = q.poll();

			for (int k = 0; k < 4; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];

				if (isRange(nr, nc, arr.length, arr[0].length) && arr[nr][nc] < cur[2]) {
					if (visit[nr][nc] == 0) {
						q.add(new int[] { nr, nc, arr[nr][nc] });
					}

					visit[nr][nc] += visit[cur[0]][cur[1]];
				}

			}

		}

//		for(int i[] : visit)
//			System.out.println(Arrays.toString(i));
		
		return visit[arr.length - 1][arr[0].length - 1];

	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}

}
