import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_15653_����Ż��4 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int arr[][] = new int[N][M];
		int[] blue = null, red = null;

		Queue<int[]> q = new LinkedList<>();
		for (int i = 0; i < N; i++) {
			String input = br.readLine();
			for (int j = 0; j < M; j++) {
				arr[i][j] = input.charAt(j) == '#' ? 1 : 0;

				if (input.charAt(j) == 'B') {
					blue = new int[] { i, j };
				}
				if (input.charAt(j) == 'R') {
					red = new int[] { i, j };
				}
				if (input.charAt(j) == 'O') {
					arr[i][j] = 2;
				}
			}
		}

		q.add(new int[] { blue[0], blue[1], red[0], red[1], 0 });
		boolean visit[][][][] = new boolean[N][M][N][M];
//		print(arr);
		bw.write(BFS(arr, q, visit) + "");
		bw.flush();
	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static int BFS(int arr[][], Queue<int[]> q, boolean visit[][][][]) {
		Queue<int[]> q2 = new LinkedList<>();

		while (!q.isEmpty()) {
			int cur[] = q.poll();

			for (int k = 0; k < 4; k++) {
				boolean isRedGoal = false;
				int bnr = cur[0];
				int bnc = cur[1];
				int rnr = cur[2];
				int rnc = cur[3];
				visit[bnr][bnc][rnr][rnc] = true;
				while (true) {
//					System.out.println(bnr + ", " + bnc + " / " + rnr + " , " + rnc);
					boolean isBlueMove = true;
					boolean isRedMove = true;
					bnr += delta[k][0];
					bnc += delta[k][1];
					if (!isRedGoal) {
						rnr += delta[k][0];
						rnc += delta[k][1];
					}
					if (arr[bnr][bnc] == 1) {
						bnr -= delta[k][0];
						bnc -= delta[k][1];
						isBlueMove = false;
					}
					if (arr[rnr][rnc] == 1) {
						rnr -= delta[k][0];
						rnc -= delta[k][1];
						isRedMove = false;
					}
					if (arr[bnr][bnc] == 2) {
						isRedGoal = false;
						break;
					}
					if (arr[rnr][rnc] == 2) {
						isRedGoal = true;
					}
					if (rnr == bnr && rnc == bnc) {
						if (isBlueMove) {
							bnr -= delta[k][0];
							bnc -= delta[k][1];
							isBlueMove = false;

						} else {
							rnr -= delta[k][0];
							rnc -= delta[k][1];
							isRedMove = false;

						}
					}
					if ((!isRedMove || isRedGoal) && !isBlueMove) {
						if (!visit[bnr][bnc][rnr][rnc])
							q.add(new int[] { bnr, bnc, rnr, rnc, cur[4] + 1 });
						break;
					}

				}
				if (isRedGoal) {
					return cur[4] + 1;
				}
				q2.clear();
			}

		}
		return -1;
	}

	static boolean isRange(int bnr, int bnc, int rnr, int rnc, int R, int C) {
		return rnr >= 0 && rnc >= 0 && rnr < R && rnc < C && bnr >= 0 && bnc >= 0 && bnr < R && bnc < C;
	}

	static void print(int arr[][]) {
		for (int i[] : arr)
			System.out.println(Arrays.toString(i));
		System.out.println();
	}
}
