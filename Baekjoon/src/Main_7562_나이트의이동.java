import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_7562_나이트의이동 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st;

		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {
			answer = 0;
			q = new LinkedList<>();
			int N = Integer.parseInt(br.readLine());
			st = new StringTokenizer(br.readLine());
			boolean visit[][] = new boolean[N][N];
			q.add(new int[] { Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), 0 });
			st = new StringTokenizer(br.readLine());
			int target[] = new int[] { Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()) };
			bfs(target, N, visit);
			bw.write(answer + "");
			bw.newLine();
		}
		bw.flush();

	}

	static Queue<int[]> q;
	static int answer;
	static int delta[][] = { { -2, -1 }, { -2, 1 }, { -1, -2 }, { -1, 2 }, { 2, -1 }, { 1, -2 }, { 2, 1 }, { 1, 2 } };

	static boolean isRange(int nr, int nc, int N) {
		return nr >= 0 && nc >= 0 && nr < N && nc < N;
	}

	static void bfs(int[] target, int N, boolean[][] visit) {
		int s[] = q.peek();
		
		visit[s[0]][s[1]] = true;
		while (!q.isEmpty()) {

			int cur[] = q.poll();
			for (int k = 0; k < delta.length; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];

				if(isRange(nr, nc, N) && !visit[nr][nc]) {
					
					if(target[0] == nr && target[1] == nc) {
						answer = cur[2] + 1;
						return;
					}
					visit[nr][nc] = true;
					q.add(new int[] {nr,nc,cur[2]+1});
				}
			}

		}
	}
}
