import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_4991_�κ�û�ұ� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		while (true) {
			answer = Integer.MAX_VALUE;
			StringTokenizer st = new StringTokenizer(br.readLine());
			int W = Integer.parseInt(st.nextToken());
			int H = Integer.parseInt(st.nextToken());

			if (H == 0) {
				break;
			}
			arr = new char[H][W];
			robot = new int[2];
			Queue<int[]> q = new LinkedList<>();
			for (int i = 0; i < H; i++) {
				String input = br.readLine();
				for (int j = 0; j < W; j++) {
					arr[i][j] = input.charAt(j);
					if (arr[i][j] == 'o') {
						robot[0] = i;
						robot[1] = j;
					}
					if (arr[i][j] == '*') {
						q.add(new int[] { i, j });
					}
				}

			}

			dust = new int[q.size() + 1][2];
			dust[0] = robot;
			int idx = 1;
			while (!q.isEmpty()) {
				dust[idx++] = q.poll();
			}
			int[][] dis = calcDistance(dust);
			if (dis[0][0] == -1)
				bw.write(-1 + "");
			else {
				int perm[] = new int[dust.length - 1];
				for(int i = 0 ; i < dust.length - 1; i++) {
					perm[i] = i+1;
				}
				perm(perm, 0, dis);
				
				bw.write(answer + "");
				}
			bw.newLine();
			bw.flush();
		}
	}
	
	static int calcPerm(int perm[], int[][] dis) {
		int res = dis[0][perm[0]];
		for(int i = 1 ; i < perm.length ; i++) {
			res += dis[perm[i-1]][perm[i]];
		}
		
		return res;
	}

	static void perm(int perm[], int idx, int[][] dis) {
		if (idx == perm.length) {
			int res = calcPerm(perm, dis);
			if(answer > res) {
				answer = res;
			}
			return;
		}

		for (int i = idx; i < perm.length; i++) {
			int tmp = perm[idx];
			perm[idx] = perm[i];
			perm[i] = tmp;
			perm(perm, idx + 1, dis);
			tmp = perm[idx];
			perm[idx] = perm[i];
			perm[i] = tmp;
		}

	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}

	static int[][] calcDistance(int[][] dust) {
		int[][] dis = new int[dust.length][dust.length];
		Queue<int[]> q = new LinkedList<>();
		for (int i = 0; i < dust.length; i++) {

			int start[] = dust[i];
			for (int j = 0; j < dust.length; j++) {
				if (i == j) {
					continue;
				}
				int end[] = dust[j];

				q.add(new int[] { start[0], start[1], 0 });
				boolean visit[][] = new boolean[arr.length][arr[0].length];
				visit[start[0]][start[1]] = true;
				bfs: while (!q.isEmpty()) {

					int cur[] = q.poll();
					for (int k = 0; k < 4; k++) {
						int nr = cur[0] + delta[k][0];
						int nc = cur[1] + delta[k][1];
						if (isRange(nr, nc, arr.length, arr[0].length) && !visit[nr][nc] && arr[nr][nc] != 'x') {
							if (nr == end[0] && nc == end[1]) {
								dis[i][j] = cur[2] + 1;
								dis[j][i] = cur[2] + 1;
								q.clear();
								break bfs;
							}
							q.offer(new int[] { nr, nc, cur[2] + 1 });
							visit[nr][nc] = true;
						}
					}

				}
				if (dis[i][j] == 0) {
					dis[0][0] = -1;
					return dis;
				}
			}
		}

		return dis;
	}

	static char arr[][];
	static int dust[][];
	static int robot[];
	static int answer = Integer.MAX_VALUE;

	static int[][] delta = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static void BFS() {

	}

	static void print() {
		for (char c[] : arr)
			System.out.println(Arrays.toString(c));
		System.out.println();
	}
}
