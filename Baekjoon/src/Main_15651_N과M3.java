import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main_15651_N��M3 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());

		int arr[] = new int[N];
		for (int i = 1; i <= N; i++) {
			arr[i - 1] = i;
		}
		perm(arr, new int[C], 0, C);
		bw.flush();
	}
	static BufferedWriter bw;
	static void perm(int arr[], int res[], int idx, int max) throws IOException {

		if (idx == max) {
			for (int i : res)
				bw.write(i + " ");
			bw.newLine();
			return;
		}
		
		for(int i = 0 ; i < arr.length; i++) {
			res[idx] = arr[i];
			perm(arr, res, idx+1, max);
			
		}

	}
}
