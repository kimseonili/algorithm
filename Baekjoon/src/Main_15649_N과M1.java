import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_15649_N��M1 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		int arr[] = new int[N];
		for(int i = 0 ; i < N ; i++)
			arr[i] = i+1;
		perm(arr, 0, C, new boolean[N], new int[C]);
		
	}
	
	static void perm(int arr[], int idx, int max, boolean check[], int res[]) {
		
		if(idx == max) {
			for(int i : res)
				System.out.print(i + " ");
			System.out.println();
			return;
		}
		
		for(int i = 0 ; i < arr.length ; i++) {
			if(!check[i]) {
				check[i] = true;
				
				res[idx] = arr[i];
				perm(arr, idx+1,  max, check, res);
				check[i] = false;
			}
		}
	}
}
