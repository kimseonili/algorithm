import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_1062_����ħ {

	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int N = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());
		
		int arr[] = new int[26];
		arr['a' - 'a'] = 1;
		arr['n' - 'a'] = 1;
		arr['t' - 'a'] = 1;
		arr['i' - 'a'] = 1;
		arr['c' - 'a'] = 1;
		str  = new String[N];
		for(int i = 0 ; i < N ; i++) {
			str[i] = br.readLine();
		}
		comb(arr, 0, K-5);
		bw.write(answer + "");
		bw.flush();
	}
	
	static String str[];
	static int answer = 0;
	static void comb(int arr[], int idx, int k) {
		if(k == 0) {
			int res = 0;
			outer : for(int i = 0 ; i < str.length ; i++) {
				for(int j = 3 ; j < str[i].length() - 4 ; j++) {
					if(arr[str[i].charAt(j) -'a'] == 0)
						continue outer;
				}
				res++;
			}
			if(answer < res) {
				answer = res;
			}
			return;
		}
		if(arr.length == idx)
			return;
		
		if(arr[idx] == 1) {
			comb(arr, idx+1, k);
		}
		else {
			arr[idx] = 1;
			comb(arr, idx+1, k-1);
			arr[idx] = 0;
			comb(arr, idx+1, k);
			
		}
		
		
	}
}
