import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_9328_���� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int R = Integer.parseInt(st.nextToken());
			int C = Integer.parseInt(st.nextToken());
			char[][] arr = new char[R + 2][C + 2];
			Arrays.fill(arr[0], '.');
			Arrays.fill(arr[arr.length - 1], '.');
			for (int i = 1; i < arr.length - 1; i++) {
				String input = br.readLine();
				for (int j = 0; j < arr[0].length; j++) {
					if (j == 0 || j == arr[0].length - 1) {
						arr[i][j] = '.';
					} else {
						arr[i][j] = input.charAt(j - 1);
					}
				}
			}
//			for (int i = 0 ; i < R ; i++) {
//				for(int j = 1 ; j < C + 1 ; j++) {
//					System.out.print(arr[i + 1][j]);
//				}
//				System.out.println();
//			}
//			System.out.println();
			boolean[] key = new boolean[26];
			String input = br.readLine();
			int keyCnt = 0;
			if (!input.equals("0")) {
				for (int i = 0; i < input.length(); i++) {
					if (!key[input.charAt(i) - 'a']) {
						key[input.charAt(i) - 'a'] = true;
						keyCnt++;
					}
				}
			}
			bw.write(bfs(arr, key, keyCnt) + "");
			bw.newLine();
		}
		bw.flush();
	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}

	static int bfs(char[][] arr, boolean[] key, int keyCnt) {
		int answer = 0;

		Queue<int[]> q = new LinkedList<>();
		boolean visit[][][] = new boolean[27][arr.length][arr[0].length];
		boolean get[][] = new boolean[arr.length][arr[0].length];
		visit[keyCnt][0][0] = true;
		
		q.add(new int[] { 0, 0 });
		while (!q.isEmpty()) {
			int cur[] = q.poll();
			for (int k = 0; k < 4; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];
				if (isRange(nr, nc, arr.length, arr[0].length) && !visit[keyCnt][nr][nc]) {

					if (arr[nr][nc] == '.') {
						// noting
					} else if (arr[nr][nc] == '*') {
						continue;
					} else if (arr[nr][nc] == '$') {
						if(!get[nr][nc]) {
						answer++;
						get[nr][nc] = true;}
					} else {
						if (arr[nr][nc] >= 'a') {
							if (!key[arr[nr][nc] - 'a']) {
								key[arr[nr][nc] - 'a'] = true;
								keyCnt++;
								
							}
						} else {
							int door = arr[nr][nc] - 'A';
							if (!key[door]) {
								continue;
							}
						}
					}
					visit[keyCnt][nr][nc] = true;
					q.add(new int[] { nr, nc });

				}
			}
		}

		return answer;
	}
}
