import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_1937_��������Ǵ� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st;

		int N = Integer.parseInt(br.readLine());
		int arr[][] = new int[N][N];
		dp = new int[N][N];
		for (int i = 0; i < N; i++) {

			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < N; j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		
		
		for(int i = 0 ; i < N ; i++) {
			for(int j = 0 ; j < N ; j++) {
				dfs(arr, i, j, arr[i][j]);
//				System.out.println();
//				for(int ii[] : dp)
//					System.out.println(Arrays.toString(ii));
			}
		}
		int answer= 1;
		for(int i = 0 ; i < N ; i++) {
			for(int j = 0 ; j < N ; j++) {
				if(dp[i][j] > answer)
					answer = dp[i][j];
			}
		}
		bw.write(answer + "");
		bw.flush();
	}

	static boolean isRange(int nr, int nc, int N) {
		return nr >= 0 && nc >= 0 && nr < N && nc < N;
	}

	static int dp[][];
	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static int dfs(int arr[][], int r, int c, int eat) {

		if (!isRange(r, c, arr.length)) {
			return 0;
		}

		if (dp[r][c] != 0) {
			return dp[r][c];
		}

		int tmp = 1;
		for (int k = 0; k < 4; k++) {
			int nr = r + delta[k][0];
			int nc = c + delta[k][1];

			if (isRange(nr, nc, arr.length) && eat < arr[nr][nc]) {
				int res = dfs(arr, nr, nc, arr[nr][nc]) + 1;
				if (res > tmp)
					tmp = res;
			}
		}
		dp[r][c] = tmp;
		return tmp;
	}
}
