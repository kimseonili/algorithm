import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main_1806_�κ��� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int S = Integer.parseInt(st.nextToken());

		int arr[] = new int[N];
		st = new StringTokenizer(br.readLine());
		for (int i = 0; i < N; i++)
			arr[i] = Integer.parseInt(st.nextToken());

		int s = 0;
		int e = 0;
		int sum = 0;
		int answer = Integer.MAX_VALUE;
		while (s <= N) {

			if (s == N) {
				if (sum >= S)
					if (answer > e - s)
						answer = e - s;
				break;
			}
			if (sum < S) {
				if (e == N - 1)
					break;
				sum += arr[e++];
			} else {
				if (answer > e - s)
					answer = e - s;
				sum -= arr[s++];
			}

		}
		if (answer == Integer.MAX_VALUE)
			bw.write("0");
		else
			bw.write(answer + "");
		bw.flush();
	}
}
