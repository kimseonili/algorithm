import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main_3085_사탕게임 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int N = Integer.parseInt(br.readLine());

		char[][] arr = new char[N][N];

		for (int i = 0; i < N; i++) {
			String input = br.readLine();
			for (int j = 0; j < N; j++) {
				arr[i][j] = input.charAt(j);
			}
		}
		int answer = 0;

		// 가로
		for (int i = 0; i < N; i++) {
			char ch = ' ';
			int cnt = 0;
			for (int j = 0; j < N; j++) {
				if (ch == arr[i][j]) {
					cnt++;
					if (cnt > answer)
						answer = cnt;
				} else {
					cnt = 1;
					ch = arr[i][j];
				}
			}
		}
		// 세로
		for (int i = 0; i < N; i++) {
			char ch = ' ';
			int cnt = 0;
			for (int j = 0; j < N; j++) {
				if (ch == arr[j][i]) {
					cnt++;
					if (cnt > answer)
						answer = cnt;
				} else {
					cnt = 1;
					ch = arr[j][i];
				}
			}
		}
		// swap
		for (int i = 0; i < N; i++) {

			for (int j = 0; j < N; j++) {
				// 상 하
				if (i < N - 1) {
					swap(arr, i, j, i + 1, j);
					answer = Math.max(answer, count(arr, i, j, i + 1, j));
					swap(arr, i, j, i + 1, j);
				}

				// 좌 우
				if (j < N - 1) {
					swap(arr, i, j, i, j + 1);
					answer = Math.max(answer, count(arr, i, j, i, j + 1));
					swap(arr, i, j, i, j + 1);
				}
			}

		}

		bw.write(answer + "");
		bw.flush();

	}

	static void swap(char[][] arr, int r1, int c1, int r2, int c2) {
		char tmp = arr[r1][c1];
		arr[r1][c1] = arr[r2][c2];
		arr[r2][c2] = tmp;
	}

	static int count(char[][] arr, int r1, int c1, int r2, int c2) {

		int cnt = 0;
		// 가로

		for (int i = 0, tmp = 0, ch = ' '; i < arr.length; i++) {
			if (ch == arr[r1][i]) {
				tmp++;
				if (tmp > cnt)
					cnt = tmp;
			} else {
				ch = arr[r1][i];
				tmp = 1;
			}
		}
		if (r1 != r2)
			for (int i = 0, tmp = 0, ch = ' '; i < arr.length; i++) {
				if (ch == arr[r2][i]) {
					tmp++;
					if (tmp > cnt)
						cnt = tmp;
				} else {
					ch = arr[r2][i];
					tmp = 1;
				}
			}

		// 세로

		for (int i = 0, tmp = 0, ch = ' '; i < arr.length; i++) {
			if (ch == arr[i][c1]) {
				tmp++;
				if (tmp > cnt)
					cnt = tmp;
			} else {
				ch = arr[i][c1];
				tmp = 1;
			}
		}
		if (c1 != c2)
			for (int i = 0, tmp = 0, ch = ' '; i < arr.length; i++) {
				if (ch == arr[i][c2]) {
					tmp++;
					if (tmp > cnt)
						cnt = tmp;
				} else {
					ch = arr[i][c2];
					tmp = 1;
				}
			}
		return cnt;
	}
}
