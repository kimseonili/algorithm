import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_5014_스타트링크 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int F = Integer.parseInt(st.nextToken());
		int S = Integer.parseInt(st.nextToken());
		int T = Integer.parseInt(st.nextToken());
		int U = Integer.parseInt(st.nextToken());
		int D = Integer.parseInt(st.nextToken());
		q.add(new int[] { S, 0 });
		boolean visit[] = new boolean[F + 1];
		visit[S] = true;

		delta[0] *= U;
		delta[1] *= D;
		if(S != T)
			bfs(visit, U, D, T);
		else
			answer = 0;
		
		if (answer == -1)
			bw.write("use the stairs");
		else
			bw.write(answer + "");
		bw.flush();
	}

	static int answer = -1;
	static Queue<int[]> q = new LinkedList<>();
	static int delta[] = { 1, -1 };

	static void bfs(boolean visit[], int U, int D, int T) {

		while (!q.isEmpty()) {

			int cur[] = q.poll();

			for (int k = 0; k < 2; k++) {

				int n = cur[0] + delta[k];

				if (isRange(n, visit.length) && !visit[n]) {

					if (n == T) {
						answer = cur[1] + 1;
						return;
					}

					visit[n] = true;
					q.add(new int[] { n, cur[1] + 1 });
				}

			}

		}

	}

	static boolean isRange(int n, int N) {
		return n > 0 && n < N;
	}
}
