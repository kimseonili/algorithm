import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main_5397_Ű�ΰ� {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {
			String input = br.readLine();
			TextField tf = new TextField();
			for (int i = 0; i < input.length(); i++) {
				if (input.charAt(i) == '<') {
					tf.movePrev();
				} else if (input.charAt(i) == '>') {
					tf.moveNext();
				} else if (input.charAt(i) == '-') {
					tf.remove();
				} else {
					tf.add(input.charAt(i));
				}
			}

			bw.write(tf.toString() + '\n');
		}
		bw.flush();

	}

}

class TextField {
	Node cur;
	Node head;

	public TextField() {
		head = new Node((char) 0, null, null);
		cur = head;
		cur.setFirst();
	}

	public void add(char c) {

		Node next = cur.next;
		Node nNode = new Node(c, cur, cur.next);
		cur.next = nNode;
		if (next != null)
			next.prev = nNode;
		cur = nNode;

	}

	public void remove() {
		if (cur.flag) {
			return;
		}
		Node next = cur.next;
		Node prev = cur.prev;
		cur = prev;
		cur.next = next;
		if (next != null)
			next.prev = cur;
	}

	public void moveNext() {
		if (cur.next == null)
			return;
		cur = cur.next;

	}

	public void movePrev() {
		if (cur.flag)
			return;
		cur = cur.prev;

	}

	public String toString() {
		Node node = head.next;
		StringBuffer sb = new StringBuffer();
		while (node != null) {
			sb.append(node.text);
			node = node.next;
		}
		return sb.toString();
	}
}

class Node {
	boolean flag;
	Node next;
	Node prev;
	char text;

	Node(char c, Node p, Node n) {
		text = c;
		if (p != null)
			prev = p;
		if (n != null)
			next = n;
	}

	void setFirst() {
		flag = true;
	}
}
