import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main_1991_Ʈ����ȸ {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int N = Integer.parseInt(br.readLine());
		StringTokenizer st;

		node1991 nodes[] = new node1991[N];

		char val = 'A';
		for (int i = 0; i < N; i++) {
			nodes[i] = new node1991(val++);
		}
		tree1991 tree = new tree1991(nodes[0]);
		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			int a = (st.nextToken().charAt(0) - 'A');
			char b = (st.nextToken().charAt(0));
			char c = (st.nextToken().charAt(0));
			if (b != '.') {
				nodes[a].setLeft(nodes[b - 'A']);
			}
			if (c != '.') {
				nodes[a].setRight(nodes[c - 'A']);
			}

		}

		tree.pre(tree.root);
		bw.newLine();
		tree.center(tree.root);
		bw.newLine();
		tree.post(tree.root);
		bw.flush();
	}

	static BufferedWriter bw;

	static class tree1991 {
		public tree1991(node1991 node) {
			root = node;
		}

		node1991 root;

		void pre(node1991 node) throws IOException {

			bw.write(node.val);

			if (node.left != null) {
				pre(node.left);
			}
			if (node.right != null) {
				pre(node.right);
			}

		}

		void center(node1991 node) throws IOException {

			if (node.left != null) {
				center(node.left);
			}
			bw.write(node.val);
			if (node.right != null) {
				center(node.right);
			}
		}

		void post(node1991 node) throws IOException {

			if (node.left != null) {
				post(node.left);
			}
			if (node.right != null) {
				post(node.right);
			}
			bw.write(node.val);
		}

	}

	static class node1991 {

		char val;
		node1991 left;
		node1991 right;

		node1991(char val) {
			this.val = val;
		}

		void setLeft(node1991 left) {
			this.left = left;
		}

		void setRight(node1991 right) {
			this.right = right;
		}

		public String toString() {
			return this.val + ", " + this.left + " : " + this.right;
		}
	}
}
