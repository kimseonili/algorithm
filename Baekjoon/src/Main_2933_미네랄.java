import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_2933_�̳׶� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());

		int arr[][] = new int[R][C];

		for (int i = 0; i < R; i++) {
			String input = br.readLine();
			for (int j = 0; j < C; j++) {
				if (input.charAt(j) == 'x')
					arr[i][j] = 1;
			}
		}

		int N = Integer.parseInt(br.readLine());
		st = new StringTokenizer(br.readLine());

		int[] pit = new int[N];
		for (int i = 0; i < N; i++)
			pit[i] = Integer.parseInt(st.nextToken());
		throwStick(arr, pit);

		for (int i = 0; i < R; i++) {
			for (int j = 0; j < C; j++) {
				if (arr[i][j] == 0)
					bw.write('.');
				else
					bw.write('x');
			}
			bw.newLine();
		}
		bw.flush();

	}

	static void throwStick(int arr[][], int pit[]) {

		for (int p = 0; p < pit.length; p++) {
			boolean hit = false;
			int r = 0;
			int c = 0;
			for (int j = 0; j < arr[0].length; j++) {
				if (p % 2 == 0) {
					if (arr[arr.length - pit[p]][j] == 1) {
						r = arr.length - pit[p];
						c = j;
						arr[r][j] = 0;
						hit = true;
						break;
					}
				} else {
					if (arr[arr.length - pit[p]][arr[0].length - 1 - j] == 1) {
						r = arr.length - pit[p];
						c = arr[0].length - 1 - j;
						arr[r][c] = 0;
						hit = true;
						break;
					}
				}

			}
			if (hit) {
				for (int k = 0; k < 4; k++) {
					int nr = r + delta[k][0];
					int nc = c + delta[k][1];
					boolean visit[][] = new boolean[arr.length][arr[0].length];
					if (isRange(nr, nc, arr.length, arr[0].length) && arr[nr][nc] == 1) {
						visit[nr][nc] = true;
						q.add(new int[] { nr, nc });
						down(arr, visit);
//						System.out.println("K : " + k);
//						for(int i[] : arr)
//							System.out.println(Arrays.toString(i));
//						System.out.println();
					}
				}

			}
		}
	}

	static int[][] delta = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
	static Queue<int[]> q = new LinkedList<>();

	static void down(int arr[][], boolean[][] visit) {
		PriorityQueue<int[]> pq = new PriorityQueue<>(new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				return o2[0] - o1[0];
			}
		});
		int t[] = q.peek();
		pq.add(new int[] {t[0], t[1]});
		while (!q.isEmpty()) {

			int cur[] = q.poll();
			for (int k = 0; k < 4; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];

				if (!isRange(nr, nc, arr.length, arr[0].length)) {
					if (nr == arr.length) {
						q.clear();
						return;
					}
				} else {
					if (arr[nr][nc] == 1 && !visit[nr][nc]) {
						pq.add(new int[] { nr, nc });
						q.add(new int[] { nr, nc });
						visit[nr][nc] = true;
					}
				}

			}

		}
		Queue<int[]> tmp = new LinkedList<>();
		// check term
		int min = Integer.MAX_VALUE;
		boolean check[] = new boolean[arr[0].length];
		while (!pq.isEmpty()) {
			int cur[] = pq.poll();
//			System.out.println(Arrays.toString(cur));
			tmp.add(cur);
			int r = cur[0];
			int c = cur[1];
			if (check[c])
				continue;

			check[c] = true;

			for (int j = 1; j + r < arr.length; j++) {
				if (arr[j + r][c] == 1) {
					if (min > j - 1)
						min = j - 1;
					break;
				}
				if (j > min)
					break;
				if (j + r == arr.length - 1) {
					if (min > j)
						min = j;
				}
			}
		}
		while (!tmp.isEmpty()) {
			int cur[] = tmp.poll();
			int nr = cur[0] + min;
			int nc = cur[1];

			arr[cur[0]][cur[1]] = 0;
			arr[nr][nc] = 1;
		}

	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}
}