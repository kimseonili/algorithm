import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_12851_���ٲ���2 {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int X = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());
		int answer [] =bfs(X, K);
		bw.write(answer[0]+ " " + answer[1]);
		bw.flush();
	}
	
	static int[] bfs(int X, int K) {
		int answer = 0;
		int min = Integer.MAX_VALUE;
		Queue<int[]> q = new LinkedList<>();
		q.add(new int[] {X,0});
		
		int visit[] = new int[100001];
		Arrays.fill(visit, Integer.MAX_VALUE);
		visit[X] = 0;
		
		while(!q.isEmpty()) {
			
			int cur[] = q.poll();
			
			if(cur[0] == K) {
				
				if(min > cur[1]) {
					min = cur[1];
					answer = 1;
				}else if(min == cur[1]) {
					answer++;
				}else {
					continue;
				}
				
			}
			
			if(cur[0] > 0 && visit[cur[0] - 1] >= cur[1] + 1) {
				q.add(new int[] {cur[0] - 1, cur[1] + 1});
				visit[cur[0] - 1] = cur[1] + 1;
			}
			
			if(cur[0] < visit.length - 1 && visit[cur[0]+ 1] >= cur[1] + 1) {
				q.add(new int[] {cur[0] + 1, cur[1] + 1});
				visit[cur[0] + 1] = cur[1] + 1;
			}
			if(cur[0] * 2 < visit.length && visit[cur[0] * 2] >= cur[1] + 1) {
				q.add(new int[] {cur[0] * 2, cur[1] + 1});
				visit[cur[0] * 2] = cur[1] + 1;
			}
		}
		return new int[] {min,answer};
	}
}
