import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class Main_1261_�˰����� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int C = Integer.parseInt(st.nextToken());
		int R = Integer.parseInt(st.nextToken());

		int arr[][] = new int[R][C];

		for (int i = 0; i < R; i++) {
			String input = br.readLine();
			for (int j = 0; j < C; j++) {
				arr[i][j] = input.charAt(j) - '0';
			}
		}
		bw.write(bfs(arr) + "");
		bw.flush();

	}

	static PriorityQueue<int[]> q = new PriorityQueue<int[]>(new Comparator<int[]>() {

		public int compare(int[] o1, int[] o2) {
			return o1[2] - o2[2];
		}
	});
	static int delta[][] = {{-1,0},{1,0},{0,-1},{0,1}};
	static boolean isRange(int nr, int nc, int R ,int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}
	static int bfs(int arr[][]) {
		boolean visit[][] = new boolean[arr.length][arr[0].length];
		visit[0][0] = true;
		q.add(new int[] {0,0,0});
		
		while(!q.isEmpty()) {
			int cur[] = q.poll();
			if(cur[0] == arr.length - 1 && cur[1] == arr[0].length - 1) {
				return cur[2];
			}
			for(int k = 0  ; k < 4 ; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];
				if(isRange(nr, nc, arr.length, arr[0].length) && !visit[nr][nc]) {
					visit[nr][nc] = true;
					if(arr[nr][nc] == 1) {
						q.add(new int[] {nr,nc,cur[2] + 1});
					}else {
						q.add(new int[] {nr,nc,cur[2]});
						
					}
				}
			}
		}
		
		return -1;
	}
}
