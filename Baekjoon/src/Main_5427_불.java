import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_5427_�� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st;

		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {
			st = new StringTokenizer(br.readLine());
			int C = Integer.parseInt(st.nextToken());
			int R = Integer.parseInt(st.nextToken());
			q = new LinkedList<>();
			char arr[][] = new char[R][C];
			boolean visit[][] = new boolean[R][C];
			int[] start = null;
			for (int i = 0; i < R; i++) {
				String input = br.readLine();

				for (int j = 0; j < C; j++) {

					arr[i][j] = input.charAt(j);
					if (arr[i][j] == '@') {
						visit[i][j] = true;
						start = new int[] {i,j,0,0};
					} else if (arr[i][j] == '*') {
						visit[i][j] = true;
						q.add(new int[] { i, j, 1, 0 });
					}
				}

			}
			q.add(start);
			BFS(arr, visit);
			if (answer == -1)
				bw.write("IMPOSSIBLE");

			else
				bw.write(answer + "");
			bw.newLine();
		}
		bw.flush();
	}

	static Queue<int[]> q;
	static int answer;
	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static boolean isExit(int nr, int nc, int R, int C) {
		return (nr < 0 || nc < 0 || nr >= R || nc >= C);
	}

	static void BFS(char arr[][], boolean[][] visit) {

		while (!q.isEmpty()) {

			int cur[] = q.poll();

			for (int k = 0; k < 4; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];

				if (isExit(nr, nc, arr.length, arr[0].length)) {
					if (cur[2] == 0) {
						answer = cur[3] + 1;
						return;
					} else {
						continue;
					}
				}
				if (!visit[nr][nc] && (arr[nr][nc] == '.' || arr[nr][nc] == '@')) {
					visit[nr][nc] = true;
					q.add(new int[] { nr, nc, cur[2], cur[3] + 1 });
				}

			}

		}
		answer = -1;
	}
}
