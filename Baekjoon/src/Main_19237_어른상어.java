import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_19237_���� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());

		int arr[][][] = new int[N][N][3];
		Queue<int[]> q = new LinkedList<>();
		int initialShark[][] = new int[M][2];
		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < N; j++) {
				int cur = Integer.parseInt(st.nextToken());
				if (cur != 0) {
					arr[i][j][1] = K;
					initialShark[cur - 1] = new int[] { i, j };
				}
				arr[i][j][0] = cur;
			}
		}
		int sharkDir[] = new int[M];
		st = new StringTokenizer(br.readLine());
		for (int i = 0; i < M; i++) {
			sharkDir[i] = Integer.parseInt(st.nextToken()) - 1;
		}

		int shark[][][] = new int[M][4][4];

		for (int i = 0; i < M; i++) {
			for (int j = 0; j < 4; j++) {
				st = new StringTokenizer(br.readLine());
				for (int k = 0; k < 4; k++) {
					shark[i][j][k] = Integer.parseInt(st.nextToken()) - 1;
				}
			}
		}
		for (int i = 0; i < initialShark.length; i++) {
			q.add(new int[] { initialShark[i][0], initialShark[i][1], i + 1 });
		}
		int answer = BFS(arr, sharkDir, shark, q, K);
		bw.write(answer + "");
		bw.flush();

	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static int BFS(int[][][] arr, int[] sharkDir, int[][][] shark, Queue<int[]> q, int K) {
		int t = sharkDir.length;
		t = 0;
		int left = sharkDir.length;
		while (t < 1000 * sharkDir.length) {
			if (left == 1) {
				if(q.peek()[0] == -1)t += sharkDir.length;
				return t / sharkDir.length;
			}
			int cur[] = q.poll();
			int idx = cur[2];
			if (idx == -1) {
				q.add(new int[] { -1, -1, -1 });

			} else {
				int dir = sharkDir[idx - 1];
				boolean isGo = false;
				for (int k = 0; k < 4; k++) {
					int nr = cur[0] + delta[shark[idx - 1][dir][k]][0];
					int nc = cur[1] + delta[shark[idx - 1][dir][k]][1];
					if (isRange(nr, nc, arr.length) && (arr[nr][nc][0] == 0)) {
						q.add(new int[] { nr, nc, idx });
						arr[nr][nc] = new int[] { idx, K+1, 0 };
						sharkDir[idx - 1] = shark[idx - 1][dir][k];
						isGo = true;
						break;
					}
					if (isRange(nr, nc, arr.length) && arr[nr][nc][0] > 0 && arr[nr][nc][1] == K+1 && arr[nr][nc][2] == 0
							&& arr[nr][nc][0] != idx) {
						isGo = true;
						left--;
						q.add(new int[] { -1, -1, -1 });
						break;
					}
				}
				if (!isGo) {
					for (int k = 0; k < 4; k++) {
						int nr = cur[0] + delta[shark[idx - 1][dir][k]][0];
						int nc = cur[1] + delta[shark[idx - 1][dir][k]][1];
						if (isRange(nr, nc, arr.length) && (arr[nr][nc][0] == idx)) {
							q.add(new int[] { nr, nc, idx });
							arr[nr][nc] = new int[] { idx, K+1, 1 };
							sharkDir[idx - 1] = shark[idx - 1][dir][k];
							break;
						}
					}
				}
			}
			t++;
			if (t % sharkDir.length == 0) {
				for (int i = 0; i < arr.length; i++) {
					for (int j = 0; j < arr.length; j++) {
						if (arr[i][j][1] > 0) {
							arr[i][j][1]--;
							arr[i][j][2] = 0;
							if (arr[i][j][1] == 0) {
								arr[i][j][0] = 0;
							}
						}
					}
				}
//				System.out.println(t/sharkDir.length);
//				printMap(arr);
			}
		}
		return -1;
	}

	static boolean isRange(int nr, int nc, int N) {
		return nr >= 0 && nr < N && nc < N && nc >= 0;
	}

	static void printMap(int arr[][][]) {
		System.out.println();
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length; j++) {
				System.out.print(Arrays.toString(arr[i][j]) + " ");
			}
			System.out.println();
		}
	}
}
