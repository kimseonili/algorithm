import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_6593_������� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		while (true) {
			StringTokenizer st = new StringTokenizer(br.readLine());

			int L = Integer.parseInt(st.nextToken());
			int R = Integer.parseInt(st.nextToken());
			int C = Integer.parseInt(st.nextToken());

			if (L == 0 && C == 0 && R == 0)
				break;
			answer = Integer.MAX_VALUE;
			q = new LinkedList<>();
			char[][][] arr = new char[L][R][C];
			boolean[][][] visit = new boolean[L][R][C];

			for (int i = 0; i < L; i++) {
				for (int j = 0; j < R; j++) {
					String input = br.readLine();
					for (int k = 0; k < C; k++) {
						arr[i][j][k] = input.charAt(k);
						if (arr[i][j][k] == 'S') {
							q.add(new int[] { i, j, k, 0 });
							visit[i][j][k] = true;
						}

					}
				}
				br.readLine();
			}
			BFS(arr, visit);
			if(answer == -1)
				bw.write("Trapped!");
			else
				bw.write("Escaped in " + answer + " minute(s).");
			bw.newLine();

		}

		bw.flush();
	}

	static int answer;
	static Queue<int[]> q;
	static int delta[][] = { { -1, 0, 0 }, { 1, 0, 0 }, { 0, -1, 0 }, { 0, 1, 0 }, { 0, 0, -1 }, { 0, 0, 1 } };

	static boolean isRange(int nl, int nr, int nc, int L, int R, int C) {
		return nl >= 0 && nr >= 0 && nc >= 0 && nl < L && nc < C && nr < R;
	}

	static void BFS(char[][][] arr, boolean visit[][][]) {

		while (!q.isEmpty()) {

			int cur[] = q.poll();

			for (int k = 0; k < 6; k++) {

				int nl = cur[0] + delta[k][0];
				int nr = cur[1] + delta[k][1];
				int nc = cur[2] + delta[k][2];
				if (isRange(nl, nr, nc, arr.length, arr[0].length, arr[0][0].length) && !visit[nl][nr][nc]) {

					if (arr[nl][nr][nc] == '.') {
						visit[nl][nr][nc] = true;
						q.add(new int[] { nl, nr, nc, cur[3] + 1 });
					}

					else if (arr[nl][nr][nc] == 'E') {
						answer = cur[3] + 1;
						return;
					}
				}
			}

		}
		answer = -1;
	}
}
