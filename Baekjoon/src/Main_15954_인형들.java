import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main_15954_인형들 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());
		st = new StringTokenizer(br.readLine());
		int arr[] = new int[N];
		for (int i = 0; i < N; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		double answer = Double.MAX_VALUE;
		for (int i = 0; i <= N - K; i++) {

			for (int c = K; c + i <= N; c++) {
				int sum = 0;
				
				for (int j = 0; j < c; j++) {
					sum += arr[i + j];
//					System.out.print(i + j + " ");
				}
//				System.out.println(":: C " + c);
				double avg = (double) sum / c;
				double b = 0;
				for (int j = 0; j < c; j++) {
					b += ((double) arr[i + j] - avg) * ((double) arr[i + j] - avg);
				}
				b /= c;
				double p = Math.sqrt(b);
				if (answer > p)
					answer = p;
//			System.out.println("AVG : " + avg + " :: " + "분산 : " + b + " :: 표준편차 : " + p);
//			System.out.printf("AVG : " + avg + " :: " + "분산 : %6f" + " :: 표준편차 : %6f", b,p);
//			System.out.println();
			}
		}
		bw.write(Double.toString(answer));
		bw.flush();
	}
}
