import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_16235_나무재테크 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());

		int[][] tree = new int[M][3];
		int add[][] = new int[N][N];
		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < N; j++) {
				add[i][j] = Integer.parseInt(st.nextToken());
			}
		}

		for (int i = 0; i < M; i++) {
			st = new StringTokenizer(br.readLine());
			int x = Integer.parseInt(st.nextToken());
			int y = Integer.parseInt(st.nextToken());
			int z = Integer.parseInt(st.nextToken());
			tree[i][0] = x - 1;
			tree[i][1] = y - 1;
			tree[i][2] = z;
		}
		Arrays.sort(tree, new Comparator<int[]>() {

			@Override
			public int compare(int[] o1, int[] o2) {
				// TODO Auto-generated method stub
				return o1[2] - o2[2];
			}

		});
		int answer = simulate(add, tree, K);
		bw.write(answer + "");
		bw.flush();
	}

	static int simulate(int add[][], int[][] tree, int K) {
		int arr[][] = new int[add.length][add.length];

		LinkedList<Integer> treeArr[][] = new LinkedList[add.length][add.length];
		Queue<int[]> treeLocation = new LinkedList<>();

		for (int i[] : arr)
			Arrays.fill(i, 5);
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length; j++) {
				treeArr[i][j] = new LinkedList<>();
			}
		}
		for (int i = 0; i < tree.length; i++) {
			int r = tree[i][0];
			int c = tree[i][1];

			if (treeArr[r][c].size() == 0) {
				treeLocation.add(new int[] { r, c });
			}
			treeArr[r][c].add(tree[i][2]);
		}

		for (int y = 0; y < K; y++) {
			// 봄

			int len = treeLocation.size();
			for (int i = 0; i < len; i++) {

				int cur[] = treeLocation.poll();
				int r = cur[0];
				int c = cur[1];
				LinkedList<Integer> treeList = treeArr[r][c];
				int rice = 0;
				int size = treeList.size();
				for (int j = 0; j < size; j++) {
					int tr = treeList.poll();
					if (arr[r][c] >= tr) {
						arr[r][c] -= tr;
						treeList.add(tr + 1);
					} else {
						rice += tr / 2;
					}
				}
				arr[r][c] += rice;
				if (treeList.size() > 0) {
					treeLocation.add(cur);

				}

			}
//			for(int i : treeArr[0][0])
//				System.out.println("나이 : " + i);
//			System.out.println();
			// 가을
			len = treeLocation.size();
			for (int i = 0; i < len; i++) {
				int cur[] = treeLocation.poll();
				int r = cur[0];
				int c = cur[1];
				LinkedList<Integer> treeList = treeArr[r][c];
				int size = treeList.size();
				for (int j = 0; j < size; j++) {
					int tr = treeList.poll();
					if (tr % 5 == 0) {
						for (int k = 0; k < delta.length; k++) {
							int nr = r + delta[k][0];
							int nc = c + delta[k][1];
							if (isRange(nr, nc, arr.length)) {
								if (treeArr[nr][nc].size() == 0) {
									treeLocation.add(new int[] { nr, nc });
								}
								treeArr[nr][nc].addFirst(1);
							}
						}
					}
					treeList.add(tr);
				}
				treeLocation.add(cur);
			}

			// 겨울

			for (int i = 0; i < arr.length; i++) {
				for (int j = 0; j < arr.length; j++) {
					arr[i][j] += add[i][j];
				}
			}

			// 나이먹음

		}

		// 카운팅

		int sum = 0;
		while (!treeLocation.isEmpty()) {
			int cur[] = treeLocation.poll();
			sum += treeArr[cur[0]][cur[1]].size();
		}
		return sum;
	}

	static int delta[][] = { { -1, -1 }, { -1, 0 }, { -1, 1 }, { 0, -1 }, { 0, 1 }, { 1, -1 }, { 1, 0 }, { 1, 1 } };

	static boolean isRange(int nr, int nc, int N) {
		return nr >= 0 && nc >= 0 && nr < N && nc < N;
	}
}
