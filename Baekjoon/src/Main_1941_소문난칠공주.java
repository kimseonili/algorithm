import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Main_1941_�ҹ���ĥ���� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		char[][] arr = new char[5][5];

		for (int i = 0; i < 5; i++) {
			String input = br.readLine();
			for (int j = 0; j < 5; j++) {
				arr[i][j] = input.charAt(j);
			}
		}
		DFS(0, 0, false, 0, 0, 0, new boolean[5][5], arr);
		bw.flush();
		System.out.println(answer);
	}

	static int answer = 0;
	static int delta[][] = { { 1, 0 }, { -1, 0 }, { 0, -1 }, { 0, 1 } };

	static boolean isRange(int nr, int nc) {
		return nr >= 0 && nc >= 0 && nr < 5 && nc < 5;
	}

	static boolean isNear(int r, int c, int nr, int nc) {
		for (int i = 0; i < delta.length; i++) {
			if (r + delta[i][0] == nr && c + delta[i][1] == nc)
				return true;
		}
		return false;
	}

	static void DFS(int r, int c, boolean flag, int fr, int fc, int count, boolean check[][], char arr[][]) {
		if (count == 7) {

			Queue<int[]> q = new LinkedList<>();
			q.add(new int[] { fr, fc });
			boolean visit[][] = new boolean[5][5];
			visit[fr][fc] = true;
			int cnt = 1;
			int s = 0;
			if (arr[fr][fc] == 'S')
				s += 1;
			while (!q.isEmpty()) {

				int cur[] = q.poll();
				for (int k = 0; k < delta.length; k++) {
					int nr = cur[0] + delta[k][0];
					int nc = cur[1] + delta[k][1];
					if (isRange(nr, nc) && check[nr][nc] && !visit[nr][nc]) {
						if (arr[nr][nc] == 'S')
							s += 1;
						q.add(new int[] { nr, nc });
						visit[nr][nc] = true;
						cnt += 1;
					}
				}
			}
			if (cnt == 7 && s > 3) {
				answer++;
//				for (boolean b[] : check)
//					System.out.println(Arrays.toString(b));
//				System.out.println();
			}
			return;
		}
		if (r == 5)
			return;
		if (!flag) {
			fr = r;
			fc = c;
		}
		if (c == 4) {
			check[r][c] = true;
			DFS(r + 1, 0, true, fr, fc, count + 1, check, arr);
			check[r][c] = false;
			DFS(r + 1, 0, flag, fr, fc, count, check, arr);
		} else {
			check[r][c] = true;
			DFS(r, c + 1, flag, fr, fc, count + 1, check, arr);
			check[r][c] = false;
			DFS(r, c + 1, flag, fr, fc, count, check, arr);

		}

	}
}
