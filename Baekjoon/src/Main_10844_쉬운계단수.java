import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Main_10844_쉬운계단수 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		// 수의 길이
		int N = Integer.parseInt(br.readLine());
		
		//맨 뒷자리 숫자별 갯수를 셀 배열
		int lastNum[][] = new int[N][10];
		
		//1~9까지 개수를 1개로 지정함(1,2,3,4,5,6,7,8,9)
		Arrays.fill(lastNum[0], 1);
		//0으로 시작하는 숫자는 없으므로
		lastNum[0][0] = 0;
		
		for(int loop = 0 ; loop < N - 1 ; loop++) {
			
			for(int num = 0; num < 10 ; num++) {
				
				//마지막자리가 0보다 클 때 
				if(num - 1 >= 0) {
					lastNum[loop+1][num-1] = (lastNum[loop][num] + lastNum[loop+1][num-1]) % 1000000000;
				}
				
				//마지막자리가 9보다 작을 때
				if(num + 1 < 10) {
					lastNum[loop+1][num +1] = (lastNum[loop][num] + lastNum[loop+1][num +1]) % 1000000000;
				}
				
			}
			
		}

		// 정답을 저장할 공간
		int answer = 0;
		for(int num = 0 ; num < 10 ; num++) {
			answer =( answer + lastNum[N - 1][num] )% 1000000000;
			
		}
		System.out.println(answer);

	}
}
