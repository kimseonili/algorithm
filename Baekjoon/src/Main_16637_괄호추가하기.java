import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;

public class Main_16637_괄호추가하기 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int N = Integer.parseInt(br.readLine());

		String input = br.readLine();
		answer = 0;

		int arr[] = new int[(input.length() + 1) / 2];
		char op[] = new char[(input.length()) / 2];
		for (int i = 0; i < input.length(); i++) {
			if (i % 2 == 0) {
				arr[i / 2] = input.charAt(i) - '0';
			} else {
				op[i / 2] = input.charAt(i);
			}
		}
		dfs(new boolean[op.length], arr, op, 0, 0);
//		System.out.println(Arrays.toString(arr));
//		System.out.println(Arrays.toString(op));
		bw.write(answer + "");
		bw.flush();
		
	}

	static int answer;

	static void dfs(boolean check[], int arr[], char[] op, int idx, int count) {
		if (idx >= check.length) {
			// 값 계산 로직
			int tmp[] = new int[op.length - count + 1];
			char tmpOp[] = new char[op.length - count];
			int c = 0;
			for (int i = 0; i < arr.length; i++, c++) {

				if (i < check.length && check[i]) {
					if (op[i] == '+')
						tmp[c] = arr[i] + arr[i + 1];
					else if (op[i] == '-')
						tmp[c] = arr[i] - arr[i + 1];
					else if (op[i] == '*')
						tmp[c] = arr[i] * arr[i + 1];
					if(c < tmpOp.length)
						tmpOp[c] = op[i+1];
					i++;
				} else {
					tmp[c] = arr[i];
					if(c < tmpOp.length)
						tmpOp[c] = op[i];
				}
			}
			int res = tmp[0];
			for(int i = 0 ; i < tmpOp.length ; i++) {
				if (tmpOp[i] == '+')
					res += tmp[i+1];
				else if (tmpOp[i] == '-')
					res -= tmp[i+1];
				else if (tmpOp[i] == '*')
					res *= tmp[i+1];
			}
//			System.out.println(Arrays.toString(check));
//			System.out.println(Arrays.toString(tmp));
//			System.out.println(Arrays.toString(tmpOp));
//			System.out.println(res);
			if (answer < res)
				answer = res;
			return;
		}
		// 괄호 추가
		check[idx] = true;
		if(idx  <  check.length)
		dfs(check, arr, op, idx + 2, count + 1);
		check[idx] = false;
		// 괄호 미추가
		dfs(check, arr, op, idx + 1, count);
	}
}
