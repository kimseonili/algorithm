import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_1932_정수삼각형 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		// 정수삼각형 한 변의 길이
		int triangleSize = Integer.parseInt(br.readLine());

		// 정수삼각형의 원소를 담은 배열
		int triangle[][] = new int[triangleSize][];
		// 정수삼각형의 경로를 따라 내려왔을때 최댓값을 저장하는 배열
		int maxRouteValue[][] = new int[triangleSize][];
		// 정수삼각형의 경로를 이미 탐색했는지 여부를 저장하는 배열
		boolean isVisited[][] = new boolean[triangleSize][];
		/*
		 * level : 정수삼각형의 층
		 */
		for (int level = 0; level < triangleSize; level++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			// 정수삼각형의 해당하는층의 길이
			int levelLen = st.countTokens();
			triangle[level] = new int[levelLen];
			maxRouteValue[level] = new int[levelLen];
			isVisited[level] = new boolean[levelLen];
			for (int index = 0; index < levelLen; index++) {
				triangle[level][index] = Integer.parseInt(st.nextToken());
			}
		}
		// 시작점 경로의 최댓값을 초기화함
		maxRouteValue[0][0] = triangle[0][0];
		// 정수삼각형의 경로를 bfs로 탐색하기위한 Queue
		Queue<int[]> bfsQueue = new LinkedList<>();
		// 출발점인 0,0을 Queue에 최초로 추가
		bfsQueue.add(new int[] { 1, 0 });
		bfsQueue.add(new int[] { 1, 1 });

		while (!bfsQueue.isEmpty()) {
			// 큐에서 뽑아낸 좌표
			int currentPoint[] = bfsQueue.poll();

			int currentLevel = currentPoint[0];
			int nextLevel = currentLevel + 1;

			int currentCol = currentPoint[1];
			int leftCol = currentPoint[1];
			int rightCol = currentPoint[1] + 1;

			int rightUpRouteValue = 0;
			int leftUpRouteValue = 0;

			// 삼각형의 맨 밑까지 탐색했다면 탐색 종료 후 다음경로 탐색
			// 이미 경로계산이 끝난 지점이라면 다음경로 탐색
			if (currentLevel == triangleSize || isVisited[currentLevel][currentCol]) {
				continue;
			}
			isVisited[currentLevel][currentCol] = true;
			// 탐색할 경로의 열이 1 이상이라면(왼쪽상단 경로가 존재한다면)
			if (currentCol > 0) {
				leftUpRouteValue = maxRouteValue[currentLevel - 1][currentCol - 1];
			}
			// 탐색할 경로의 열이 가장끝이 아니라면(오른쪽 상단 경로가 존재한다면)
			if (currentCol < currentLevel) {
				rightUpRouteValue = maxRouteValue[currentLevel - 1][currentCol];
			}

			// 좌우상단의 값을 비교하여 더 큰값의 경로를 가져와 현재위치의 수를 더함
			maxRouteValue[currentLevel][currentCol] = Math.max(leftUpRouteValue, rightUpRouteValue)
					+ triangle[currentLevel][currentCol];
			bfsQueue.add(new int[] { nextLevel, leftCol });
			bfsQueue.add(new int[] { nextLevel, rightCol });
		}
		int answer = 0;
		for (int index = 0; index < triangleSize; index++) {
			answer = Math.max(answer, maxRouteValue[triangleSize - 1][index]);
		}
		System.out.println(answer);
	}
}
