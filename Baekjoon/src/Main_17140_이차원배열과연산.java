import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;

public class Main_17140_이차원배열과연산 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int arr[][] = new int[100][100];
		int R = Integer.parseInt(st.nextToken()) - 1;
		int C = Integer.parseInt(st.nextToken()) - 1;
		int K = Integer.parseInt(st.nextToken());
		for (int i = 0; i < 3; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < 3; j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		int cnt = 0;
		int[] len = new int[] { 3, 3 };
		while (arr[R][C] != K) {
			func(arr, len);
			System.out.println(Arrays.toString(len));
//			for (int i = 0; i < len[0]; i++) {
//				for (int j = 0; j < len[1]; j++) {
//					System.out.print(arr[i][j] + " ");
//				}
//				System.out.println();
//			}
			cnt++;
			if (cnt == 100) {
				cnt = -1;
				break;
			}
		}
		bw.write(Integer.toString(cnt));
		bw.flush();
	}

	static Comparator<int[]> comp = new Comparator<int[]>() {
		@Override
		public int compare(int[] o1, int[] o2) {
			if (o1[1] == o2[1]) {
				if (o1[0] < o2[0]) {
					return -1;
				} else if (o1[0] > o2[0]) {
					return 1;
				}
				return 0;
			}

			if (o1[1] < o2[1]) {
				return -1;
			} else if (o1[1] > o2[1]) {
				return 1;
			}
			return 0;
		}

	};

	static void func(int arr[][], int[] len) {
		if (len[0] >= len[1]) {
			for (int i = 0; i < len[0]; i++) {
				int hash[][] = new int[101][2];
				for (int k = 0; k < hash.length; k++) {
					hash[k][0] = k;
				}
				for (int j = 0; j < len[1]; j++) {
					if (arr[i][j] > 0)
						hash[arr[i][j]][1]++;
				}
				Arrays.sort(hash, comp);
				int idx = 0;
				for (int k = 0; k < hash.length; k++) {
					if (hash[k][1] > 0) {
						if (idx == 101)
							break;
						arr[i][idx++] = hash[k][0];
						if (idx == 101)
							break;
						arr[i][idx++] = hash[k][1];
					}
				}
				if (idx > len[1]) {
					len[1] = idx;
				}
				for(; idx < len[1] ; idx++) {
					arr[i][idx] = 0;
				}
			}
		} else {
			for (int j = 0; j < len[1]; j++) {
				int hash[][] = new int[101][2];
				for (int k = 0; k < hash.length; k++) {
					hash[k][0] = k;
				}
				for (int i = 0; i < len[0]; i++) {
					if (arr[i][j] > 0)
						hash[arr[i][j]][1]++;
				}
				Arrays.sort(hash, comp);
				int idx = 0;
				for (int k = 0; k < hash.length; k++) {
					if (hash[k][1] > 0) {
						if (idx == 101)
							break;
						arr[idx++][j] = hash[k][0];
						if (idx == 101)
							break;
						arr[idx++][j] = hash[k][1];
					}
				}
				if (idx > len[0]) {
					len[0] = idx;
				}
				for(; idx < len[0] ; idx++)
					arr[idx][j] = 0;
			}

		}
	}

}
