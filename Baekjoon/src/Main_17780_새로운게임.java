import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_17780_새로운게임 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());
		int arr[][] = new int[N + 2][N + 2];
		for (int i[] : arr)
			Arrays.fill(i, 2);

		int horse[][] = new int[K][5];
		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < N; j++) {
				arr[i + 1][j + 1] = Integer.parseInt(st.nextToken());
			}
		}

		for (int i = 0; i < K; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < 3; j++) {
				horse[i][j] = Integer.parseInt(st.nextToken());
			}
			horse[i][2]--;
			horse[i][3] = i;
			horse[i][4] = i;
		}
		System.out.println(BFS(arr, horse));
		
	}

	static int delta[][] = { { 0, 1 }, { 0, -1 }, { -1, 0 }, { 1, 0 } };

	static boolean isRange(int nr, int nc, int N) {
		return nr >= 0 && nc >= 0 && nr < N && nc < N;
	}

	static int BFS(int arr[][], int horse[][]) {
		int N = arr.length;
		LinkedList<int[]> chess[][] = new LinkedList[N][N];
		Queue<int[]> q = new LinkedList<>();
		for (int i[] : horse) {
			int r = i[0];
			int c = i[1];
			chess[r][c] = new LinkedList<>();
			chess[r][c].offer(i);
			q.add(i);
		}
		int turn = -1;
		while (!q.isEmpty() && ++turn / horse.length + 1 < 1001) {

			int cur[] = q.poll();
			int d = cur[2];

			if (cur[3] == cur[4]) {
				// myTurn;
			} else {
				q.add(cur);
				continue;
			}

			int nr = cur[0] + delta[d][0];
			int nc = cur[1] + delta[d][1];

			if (arr[nr][nc] == 0) {
				move(cur[0], cur[1], nr, nc, chess);
				cur[0] = nr;
				cur[1] = nc;
			} else if (arr[nr][nc] == 1) {
				reverse(cur[0], cur[1], chess);
				move(cur[0], cur[1], nr, nc, chess);
				cur[0] = nr;
				cur[1] = nc;
			} else {
				int br = cur[0] - delta[d][0];
				int bc = cur[1] - delta[d][1];
				cur[2] = cur[2] % 2 == 0 ? cur[2] + 1 : cur[2] - 1;
				if (arr[br][bc] == 1) {
					reverse(cur[0], cur[1], chess);
					move(cur[0], cur[1], br, bc, chess);
					cur[0] = br;
					cur[1] = bc;
				}
				else if(arr[br][bc] == 2) {
					//nothing
				}else {
					
					move(cur[0], cur[1], br, bc, chess);
					cur[0] = br;
					cur[1] = bc;
				}
				
			}
			System.out.println(turn);
			print(chess);
			for (int i[] : horse)
				System.out.println(Arrays.toString(i));
			System.out.println();

			if (chess[cur[0]][cur[1]].size() > 3)
				return turn / horse.length + 1;
			q.add(cur);
		}

		return -1;
	}

	static void move(int r, int c, int nr, int nc, LinkedList<int[]> chess[][]) {
		if (chess[nr][nc] == null || chess[nr][nc].size() == 0) {
			chess[nr][nc] = new LinkedList<>();
			while (!chess[r][c].isEmpty()) {
				int cur[] = chess[r][c].poll();
				cur[0] = nr;
				cur[1] = nc;
				chess[nr][nc].offer(cur);
			}
		} else {
			while (!chess[r][c].isEmpty()) {
				int[] h = chess[r][c].poll();
				h[0] = nr;
				h[1] = nc;
				h[3] = chess[nr][nc].peek()[4];
				chess[nr][nc].offer(h);
			}

		}
	}

	static void reverse(int r, int c, LinkedList<int[]> chess[][]) {
		int[][] tmp = new int[chess[r][c].size()][5];
		int base = chess[r][c].peekLast()[4];
		for (int i = 0; i < tmp.length; i++) {
			tmp[i] = chess[r][c].poll();
			tmp[i][3] = base;
		}
		for (int i = 0; i < tmp.length; i++) {
			chess[r][c].offer(tmp[tmp.length - 1 - i]);
		}

	}

	static void print(LinkedList<int[]> chess[][]) {
		for (LinkedList<int[]> i[] : chess) {
			for (LinkedList<int[]> j : i) {
				if (j == null)
					System.out.print(0 + " ");
				else
					System.out.print(j.size() + " ");
			}
			System.out.println();
		}
	}
}
