import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main_2160_�׸��� {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int N = Integer.parseInt(br.readLine());

		char arr[][][] = new char[N][5][7];

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < 5; j++) {
				arr[i][j] = br.readLine().toCharArray();
			}
		}

		int answerA = 0;
		int answerB = 0;
		int min = Integer.MAX_VALUE;
		for(int i = 0 ; i < N ; i++) {
			int diff = 0;
			for(int j = i + 1 ; j < N ; j++) {
				
				for(int ii = 0 ; ii < 5 ; ii++) {
					for(int jj = 0 ; jj < 7 ; jj++) {
						if(arr[i][ii][jj] != arr[j][ii][jj]) {
							diff++;
						}
					}
				}
				
				if(min > diff) {
					min = diff;
					answerA = i+1;
					answerB = j+1;
				}
				
			}
		}
		bw.write(answerA + " " + answerB);
		bw.flush();
	}
}
