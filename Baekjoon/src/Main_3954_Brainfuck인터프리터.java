import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_3954_Brainfuck인터프리터{

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int T = Integer.parseInt(br.readLine());
		StringTokenizer st;
		for (int t = 1; t <= T; t++) {
			st = new StringTokenizer(br.readLine());
			int M = Integer.parseInt(st.nextToken());
			int C = Integer.parseInt(st.nextToken());
			int I = Integer.parseInt(st.nextToken());

			int memory[] = new int[M];
			char code[];
			char input[];

			code = br.readLine().toCharArray();
			input = br.readLine().toCharArray();

			int loop[] = new int[code.length];
			int stack[] = new int[code.length];
			int top = -1;
			for (int i = 0; i < code.length; i++) {
				if (code[i] == '[') {
					loop[i] = i;
					stack[++top] = i;
				} else if (code[i] == ']') {
					int tmp = stack[top--];
					loop[i] = tmp;
					loop[tmp] = i;
				}

			}
			run(memory, code, input, loop, bw);
			bw.newLine();

		}
		bw.flush();
	}

	static void run(int[] memory, char[] code, char[] input, int[] loop, BufferedWriter bw) throws IOException {
		// 코드 수행 수
		int count = 0;
		// 코드 위치
		int cur = 0;
		// 포인터
		int point = 0;
		// 입력문자위치
		int letter = 0;
		// loop
		int loopStart = 0;
		int loopEnd = 0;
		while (cur < code.length) {

			switch (code[cur]) {
			case '-':
				memory[point]--;
				if (memory[point] < 0)
					memory[point] = 255;
				break;
			case '+':
				memory[point]++;
				if (memory[point] == 256)
					memory[point] = 0;
				break;
			case '<':
				point--;
				if (point < 0)
					point = memory.length - 1;
				break;
			case '>':
				point++;
				if (point == memory.length)
					point = 0;
				break;
			case '[':
				if (memory[point] == 0) {
					cur = loop[cur];
				}
				// 점프 로직
				break;
			case ']':
				if (memory[point] != 0) {
					if (loopEnd < cur) {
						loopStart = loop[cur];
						loopEnd = cur;
					}
					cur = loop[cur];
					if (count >= 50000000) {
						bw.write("Loops " + loopStart + " " + loopEnd);
						return;
					}
				}
				// 점프 로직
				break;
			case '.':
//				bw.write(memory[point] + '\n');
				break;
			case ',':
				if (letter < input.length)
					memory[point] = input[letter++];
				else
					memory[point] = 255;
				break;
			}
			cur++;
			count++;
		}
		bw.write("Terminates");
	}
}