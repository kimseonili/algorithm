import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_2174_�κ��ùķ��̼� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int C = Integer.parseInt(st.nextToken());
		int R = Integer.parseInt(st.nextToken());
		st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int arr[][] = new int[R][C];
		int robot[][] = new int[N][3];
		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			int c = Integer.parseInt(st.nextToken()) - 1;
			int r = R - Integer.parseInt(st.nextToken());
			arr[r][c] = i + 1;
			char dir = st.nextToken().charAt(0);
			int d = 0;
			switch (dir) {
			case 'E':
				d = 0;
				break;
			case 'S':
				d = 1;
				break;
			case 'W':
				d = 2;
				break;
			case 'N':
				d = 3;
				break;
			}

			robot[i] = new int[] { r, c, d };
		}
		boolean error = false;
		loop: for (int i = 0; i < M; i++) {
			st = new StringTokenizer(br.readLine());
			int no = Integer.parseInt(st.nextToken()) - 1;
			char com = st.nextToken().charAt(0);
			int loop = Integer.parseInt(st.nextToken());
			for (int j = 0; j < loop; j++) {
				for(int ii[] : arr)
					System.out.println(Arrays.toString(ii));
				System.out.println();
				int dir = robot[no][2];
				switch (com) {
				case 'F':
					arr[robot[no][0]][robot[no][1]] = 0;
					robot[no][0] += delta[dir][0];
					robot[no][1] += delta[dir][1];
					if (!isRange(robot[no][0], robot[no][1], R, C)) {
						bw.write("Robot " + (no + 1) + " crashes into the wall");
						error = true;
						break loop;
					} else if (arr[robot[no][0]][robot[no][1]] != 0) {
						bw.write("Robot " + (no + 1) + " crashes into robot " + arr[robot[no][0]][robot[no][1]]);
						error = true;
						break loop;
					}
					arr[robot[no][0]][robot[no][1]] = no + 1;
					break;
				case 'L':
					robot[no][2] = (robot[no][2] + 3) % 4;
					break;
				case 'R':
					robot[no][2] = (robot[no][2] + 1) % 4;
					break;
				}
			}
		}
		if (!error)
			bw.write("OK");
		bw.flush();
	}

	static int delta[][] = { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nc < C && nr < R;
	}
}
