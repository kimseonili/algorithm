import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_14442_벽부수고이동하기2 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());

		int[][] arr = new int[R][C];

		for (int i = 0; i < R; i++) {
			String input = br.readLine();
			for (int j = 0; j < C; j++) {
				arr[i][j] = input.charAt(j) - '0';
			}
		}

		bw.write(bfs(arr, K) + "");
		bw.flush();
	}

	static int delta[][] = { { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 } };

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;

	}

	static int bfs(int arr[][], int K) {
		int answer = Integer.MAX_VALUE;
		Queue<int[]> q = new LinkedList<>();
		boolean[][][] visit = new boolean[K+1][arr.length][arr[0].length];
		q.add(new int[] {0,0, K, 1});
		
		
		while (!q.isEmpty()) {

			int cur[] = q.poll();

			if(cur[0] == arr.length -1 && cur[1] == arr[0].length - 1) {
				if(answer > cur[3] +1 ) {
					answer = cur[3];
				}
			}
			
			for(int k = 0 ;  k < 4 ; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];
				
				if(isRange(nr, nc, arr.length, arr[0].length) && !visit[cur[2]][nr][nc]) {
					if(arr[nr][nc] == 0) {
						
						visit[cur[2]][nr][nc] = true;
						q.add(new int[] {nr,nc,cur[2], cur[3] + 1});
						
					}else {
						if(cur[2] > 0) {
							visit[cur[2]][nr][nc] = true;
							q.add(new int[] {nr,nc,cur[2] - 1, cur[3] + 1});
						}
					}
				}
			}
		}

		if(answer == Integer.MAX_VALUE)
			return -1;
		return answer;
	}
}
