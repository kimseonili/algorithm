import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_17071_���ٲ���5 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());

		int visit[] = new int[500001];
		Arrays.fill(visit, -1);
		visit[N] = 0;

		q.add(new int[] { N, 0 }); // ��ǥ, count
		bfs(visit, K);
		bw.write(answer + "");
		bw.flush();
	}

	static Queue<int[]> q = new LinkedList<>();
	static int answer = -1;

	static boolean isRange(int n, int len) {
		return n >= 0 && n < len;
	}

	static void bfs(int visit[], int K) {

		int T = K;
		int count = 0;
		while (!q.isEmpty()) {

			int cur[] = q.poll();
			if (count != cur[1]) {
				count++;
				T += count;
				if (T > 500000) {
					answer = -1;
					return;
				}
			}

			if (visit[T] == count % 2 || visit[T] == 2) {
				answer = cur[1];
				return;
			}

			for (int k = 0; k < 3; k++) {
				int n = 0;
				if (k == 0) {
					n = cur[0] * 2;
				}

				else if (k == 1) {

					n = cur[0] + 1;
				}

				else if (k == 2) {
					n = cur[0] - 1;
				}

				if (isRange(n, visit.length) && visit[n] != (cur[1] + 1) % 2 && visit[n] != 2) {
					if (visit[n] == -1)
						visit[n] = (cur[1] + 1) % 2;
					else if (visit[n] != (cur[1] + 1) % 2)
						visit[n] = 2;
					q.add(new int[] { n, cur[1] + 1 });

				}

			}

		}

	}

}
