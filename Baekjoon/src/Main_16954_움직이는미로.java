import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Main_16954_움직이는미로 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		char arr[][] = new char[8][];
		for (int i = 0; i < 8; i++) {
			String input = br.readLine();
			for (int j = 0; j < 8; j++) {
				arr[i] = input.toCharArray();
			}
		}

		int answer = BFS(arr);
		System.out.println(answer);

	}

	static int delta[][] = {{0,0 },{ -1, -1 }, { -1, 0 }, { -1, 1 }, { 0, -1 }, { 0, 1 }, { 1, -1 }, { 1, 0 }, { 1, 1 } };

	static boolean isRange(int nr, int nc) {
		return nr >= 0 && nc >= 0 && nr < 8 && nc < 8;
	}

	static int BFS(char a[][]) {
		char arr[][][] = moveMap(a);

//		for (char c[][] : arr) {
//			for (char cc[] : c) {
//				System.out.println(Arrays.toString(cc));
//			}
//			System.out.println();
//		}
		int res = 0;
		Queue<int[]> q = new LinkedList<>();
		q.add(new int[] { 7, 0, 0 });
		while (!q.isEmpty()) {
			int cur[] = q.poll();
			if (arr[cur[2]][cur[0]][cur[1]] == '#') {
				continue;
			}
			int level = cur[2] + 1;
			if (level == 8) {
				res = 1;
				break;
			}
			for (int k = 0; k < delta.length; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];
				if (isRange(nr, nc) && arr[level - 1][nr][nc] == '.') {
					q.add(new int[] { nr, nc, level });
				}
			}
		}

		return res;
	}

	static char[][][] moveMap(char[][] arr) {
		char moveArr[][][] = new char[8][8][8];

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8 - i; j++) {
				for (int k = 0; k < 8; k++) {
					moveArr[i][j + i][k] = arr[j][k];
				}
			}
		}
		return moveArr;
	}
}
