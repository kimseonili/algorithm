import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_1194_달이차오른다가자 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		char[][] arr = new char[R][C];

		for (int i = 0; i < arr.length; i++) {
			String input = br.readLine();
			for (int j = 0; j < arr[0].length; j++) {
				arr[i][j] = input.charAt(j);
				if (arr[i][j] == '0') {
					q.add(new int[] { i, j, 0, 0, 0, 0, 0, 0, 0 });
					arr[i][j] = '.';
				}
			}

		}
		bw.write(bfs(arr) + "");
		bw.flush();

	}

	static Queue<int[]> q = new LinkedList<>();
	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}

	static int bfs(char[][] arr) {
		int answer = Integer.MAX_VALUE;
		int p[] = q.peek();
		boolean visit[][][] = new boolean[64][arr.length][arr[0].length];
		visit[0][p[0]][p[1]] = true;

		while (!q.isEmpty()) {
			int cur[] = q.poll();
			for (int k = 0; k < 4; k++) {
				int nr = cur[0] + delta[k][0];
				int nc = cur[1] + delta[k][1];
				int keyCnt = cur[3] *32 + cur[4] *16 + cur[5] * 8 + cur[6] * 4 + cur[7] * 2 + cur[8];
				if (isRange(nr, nc, arr.length, arr[0].length) && !visit[keyCnt][nr][nc] && arr[nr][nc] != '#') {
					if (arr[nr][nc] >= 'a' && arr[nr][nc] <= 'f') {
						boolean flag = false;
						if (cur[arr[nr][nc] - 'a' + 3] == 0) {
							cur[arr[nr][nc] - 'a' + 3] = 1;
							flag = true;
							keyCnt++;
						}
						visit[keyCnt][nr][nc] = true;
						q.add(new int[] { nr, nc, cur[2] + 1, cur[3], cur[4], cur[5], cur[6], cur[7], cur[8] });
						if (flag)
							cur[arr[nr][nc] - 'a' + 3] = 0;

					} else if (arr[nr][nc] >= 'A' && arr[nr][nc] <= 'F') {

						if (cur[arr[nr][nc] - 'A' + 3] == 1) {
							visit[keyCnt][nr][nc] = true;
							q.add(new int[] { nr, nc, cur[2] + 1, cur[3], cur[4], cur[5], cur[6], cur[7], cur[8] });
						}

					} else {
						if (arr[nr][nc] == '.') {
							visit[keyCnt][nr][nc] = true;
							q.add(new int[] { nr, nc, cur[2] + 1, cur[3], cur[4], cur[5], cur[6], cur[7], cur[8] });
						} else {
							if (answer > cur[2] + 1)
								answer = cur[2] + 1;
						}
					}
				}
			}
		}
		if (answer == Integer.MAX_VALUE)
			return -1;
		return answer;
	}
}
