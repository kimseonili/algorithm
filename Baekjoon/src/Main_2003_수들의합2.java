import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main_2003_��������2 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int arr[] = new int[N];
		st = new StringTokenizer(br.readLine());
		for (int i = 0; i < N; i++)
			arr[i] = Integer.parseInt(st.nextToken());
		int answer = 0;
		int sum = arr[0];

		int s = 0;
		int e = 1;
		while (e < N || s < N) {
			if (sum < M) {
				if(e == N) break;
				sum += arr[e++];
			} else if (sum == M) {
				answer++;
				sum -= arr[s++];
			} else {
				if(s == N)break;
				sum -= arr[s++];
			}
		}
		if(sum == M) answer++;
		bw.write(answer + "");
		bw.flush();
	}
}
