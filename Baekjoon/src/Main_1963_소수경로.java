import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_1963_�Ҽ���� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {
			boolean arr[] = era();
			StringTokenizer st = new StringTokenizer(br.readLine());
			int A = Integer.parseInt(st.nextToken());
			int B = Integer.parseInt(st.nextToken());
			bw.write(Integer.toString(BFS(arr, A, B)));
			bw.newLine();
		}
		bw.flush();
	}

	static boolean[] era() {
		boolean arr[] = new boolean[10000];

		for (int i = 2; i < 10000; i++) {

			if (!arr[i])
				for (int j = i * 2; j < 10000; j += i) {
					arr[j] = true;
				}
		}

		return arr;
	}

	static int BFS(boolean arr[], int A, int B) {
		Queue<int[]> q = new LinkedList<int[]>();
		int first[] = new int[5];
		int to[] = new int[4];
		int op = 10;
		arr[A] = true;
		int t = B;
		
		for (int i = 3; i >= 0; i--) {
			first[i] = A % op;
			to[i] = B % op;
			A = A / op;
			B = B / op;
		}
		B = t;
		q.add(first);
		while (!q.isEmpty()) {
			int cur[] = q.poll();
			if(cur[0]* 1000 + cur[1] * 100 + cur[2] * 10 + cur[3] == B) {
				return cur[4];
			}
			for (int k = 0; k < 4; k++) {
				int tmp = cur[k];
				for (int j = 0; j < 10; j++) {
					if(k == 0 && j == 0) continue;
					cur[k] = j;
					int idx = 1000 * cur[0] + 100 * cur[1] + 10 * cur[2] + cur[3];
					if (!arr[idx]) {
						arr[idx] = true;
						q.add(new int[] { cur[0], cur[1], cur[2], cur[3], cur[4] + 1 });

					}
					cur[k] = tmp;
				}
			}
		}
		return -1;
	}
}
