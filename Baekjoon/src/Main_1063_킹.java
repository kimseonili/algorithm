import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_1063_ŷ {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());
		String tmp = st.nextToken();
		int[] king = new int[2];
		king[1] = tmp.charAt(0) - 'A';
		king[0] = 7 - (tmp.charAt(1) - '1');

		tmp = st.nextToken();
		int[] stone = new int[2];
		stone[1] = tmp.charAt(0) - 'A';
		stone[0] = 7 - (tmp.charAt(1) - '1');

		int arr[][] = new int[8][8];
		arr[stone[0]][stone[1]] = 1;
		int N = Integer.parseInt(st.nextToken());
		int cur[] = king;
		int nr = cur[0];
		int nc = cur[1];
//		System.out.println("king  : "+ Arrays.toString(cur));
//		System.out.println("stone  : " + Arrays.toString(stone));
		
		for (int i = 0; i < N; i++) {
			String command = br.readLine();
			int dr = 0;
			int dc = 0;
			switch (command) {
			case "R":
				
				dc++;
				break;
			case "L":
				dc--;
				break;
			case "B":
				dr++;
				break;
			case "T":
				dr--;
				break;
			case "RT":
				dr--;
				dc++;
				break;
			case "LT":
				dr--;
				dc--;
				break;
			case "RB":
				dr++;
				dc++;
				break;
			case "LB":
				dr++;
				dc--;
				break;
			}
			if (isRange(nr + dr, nc + dc)) {
				if (arr[nr + dr][nc + dc] == 1) {
					if (isRange(nr + dr * 2, nc + dc * 2)) {
						arr[nr + dr * 2][nc + dc * 2] = 1;
						arr[nr + dr][nc + dc] = 0;
						stone[0] += dr;
						stone[1] += dc;
						nr += dr;
						nc += dc;
					}
				} else {
					nr += dr;
					nc += dc;
				}
			}
			cur = new int[] {nr,nc};
//			System.out.println("king  : "+ Arrays.toString(cur));
//			System.out.println("stone  : " + Arrays.toString(stone));
		}
		
		String kingLoc = (char)('A'+cur[1]) +""+ (8 - cur[0]) + "";
		String stoneLoc = (char)('A'+stone[1]) +""+ (8 - stone[0]) + "";
		bw.write(kingLoc);
		bw.newLine();
		bw.write(stoneLoc);
		bw.flush();
		
	}

	static boolean isRange(int nr, int nc) {
		return nr >= 0 && nc >= 0 && nr < 8 && nc < 8;
	}
}
