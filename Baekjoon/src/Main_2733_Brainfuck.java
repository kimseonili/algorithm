import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;

public class Main_2733_Brainfuck {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		bw = new BufferedWriter(new OutputStreamWriter(System.out));
		
		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {

			StringBuilder sb = new StringBuilder("");
			while (true) {
				String input = br.readLine();

				if (input.equals("end")) {
					break;
				}

				for (int i = 0; i < input.length(); i++) {

					if (input.charAt(i) == '%') {
						break;
					}
					sb.append(input.charAt(i));

				}
			}
			int pointer[] = new int[32768];

			char[] code = sb.toString().toCharArray();
			int[] pair = new int[code.length];
			bw.write("PROGRAM #" + t + ":");
			bw.newLine();
			if (findPair(code, pair)) {
				run(pointer, code, pair);
			} else {
				bw.write("COMPILE ERROR");
			}

			bw.newLine();

		}
		bw.flush();
	}

	static BufferedWriter bw;

	static boolean findPair(char code[], int pair[]) {

		int stack[] = new int[code.length / 2];
		int top = 0;

		for (int i = 0; i < code.length; i++) {
			if (code[i] == '[') {
				stack[top++] = i;
			} else if (code[i] == ']') {
				if(top == 0)
					return false;
				int pop = stack[--top];
				pair[i] = pop;
				pair[pop] = i;
			}
		}
		if (top != 0)
			return false;
		return true;
	}

	static void run(int pointer[], char code[], int pair[]) throws IOException {
		int idx = 0;
		
		int p_idx = 0;
		while (idx < code.length) {
			switch (code[idx]) {
			case '+':
				pointer[p_idx] = (pointer[p_idx] == 255) ? 0 : pointer[p_idx] + 1;
				break;
			case '-':
				pointer[p_idx] = (pointer[p_idx] == 0) ? 255 : pointer[p_idx] - 1;
				break;
			case '>':
				p_idx = p_idx == 32767 ? 0 : p_idx + 1;
				break;
			case '<':
				p_idx = p_idx == 0 ? 32767 : p_idx - 1;
				break;
			case '.':
				bw.write(pointer[p_idx]);
				break;
			case '[':
				if (pointer[p_idx] == 0)
					idx = pair[idx];
				break;

			case ']':
				if (pointer[p_idx] != 0)
					idx = pair[idx];
				break;
			}
			idx++;
		}

	}
}
