import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;

public class Main_9663_NQueen {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		
		int N = Integer.parseInt(br.readLine());
		
		int arr[]= new int[N];
		Arrays.fill(arr, -1);
		for(int i = 0 ; i < N ; i++) {
			
			arr[0] = i;
			dfs(arr, 1);
			arr[0] = -1;
			
		}
		
		bw.write(answer + "");
		bw.flush();
		
	}
	static int answer = 0;
	static void dfs(int[] arr, int r) {
		if(r == arr.length) {
			answer++;
			return;
		}
		
		for(int j = 0 ; j < arr.length ; j++) {
			
			arr[r] = j;
			if(isPossible(arr, r )) {
				dfs(arr, r+1);
			}
			
		}
		
	}
	
	static boolean isPossible(int[] arr, int r) {
		
		//��
		for(int j = 0 ; j < r ; j++) {
			if(arr[j] == arr[r]) return false;
		}
		
		//�밢
		
		for(int i = 0 ; i < r ; i++) {
			if(Math.abs(arr[r] - arr[i]) == Math.abs( r - i)) return false;
			
		}
		
		
		return true;
	}
}
