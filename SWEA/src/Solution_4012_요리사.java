import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Solution_4012_�丮�� {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int T = Integer.parseInt(br.readLine());
		StringTokenizer st;
		for (int t = 1; t <= T; t++) {
			answer = Integer.MAX_VALUE;
			int N = Integer.parseInt(br.readLine());
			int arr[][] = new int[N][N];
			for (int i = 0; i < N; i++) {
				st = new StringTokenizer(br.readLine());
				for (int j = 0; j < N; j++) {
					arr[i][j] = Integer.parseInt(st.nextToken());
				}

			}
			comb(arr, new boolean[N], 0, 0);
			bw.write("#" + t + " " + answer + '\n');
		}
		bw.flush();
	}
	static int answer;
	static void comb(int arr[][], boolean[] check, int idx, int cnt) {

		if (idx == arr.length) {
			if(cnt != arr.length / 2)
				return;
			
//			System.out.println(Arrays.toString(check));
			int res = calc(arr, check);
			if(res < answer)
				answer = res;
			return;

		}

		if (cnt < arr.length / 2) {
			check[idx] = true;
			comb(arr, check, idx+1, cnt+1);
		}

		check[idx] = false;
		comb(arr, check, idx+1, cnt);
		
	}
	
	static int calc(int arr[][], boolean check[]) {
	
		int A = 0;
		int B = 0;
		
		for(int i = 0 ; i < arr.length ; i++) {
			
			for(int j = 0 ; j < arr.length ; j++) {
				if(i == j)
					continue;
				if(check[i] == check[j]) {
					
					if(check[i]) {
						A += arr[i][j];
					}else {
						B += arr[i][j];
					}
					
					
				}
				
			}
		}
//		System.out.println(A + " : " + B);
		return Math.abs(A-B);
	}
}