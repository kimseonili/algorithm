import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Solution_8888_Test {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		
		int Tc = Integer.parseInt(br.readLine());
		StringTokenizer st;
		for(int t = 1;  t <= Tc ; t++) {
			st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());
			int T = Integer.parseInt(st.nextToken());
			int P = Integer.parseInt(st.nextToken());
			
			int score[] = new int[T];
			int pati_score[][] = new int[N][T];
			
			for(int i = 0 ; i < N ; i++) {
				st = new StringTokenizer(br.readLine());
				
				for(int j = 0 ; j < T ; j++) {
					pati_score[i][j] = Integer.parseInt(st.nextToken());
					if(pati_score[i][j] == 0) {
						score[j]++;
					}
				}
			}
//			for(int i[] : pati_score)
//				System.out.println(Arrays.toString(i));
//			System.out.println(Arrays.toString(score));
			int myScore = 0;
			int myCount = 0;
			int myOrder = 1;
			for(int j = 0 ; j < T ; j++) {
				if(pati_score[P-1][j] == 1) {
					myScore += score[j];
					myCount++;
				}
			}
			
			for(int i = 0 ; i < pati_score.length ; i++) {
				if(i == P-1)
					continue;
				int s = 0;
				int count = 0;
				for(int j = 0 ; j < T ; j++) {
					if(pati_score[i][j] == 1) {
						s += score[j];
						count++;
					}
				}
				if(myScore < s) {
					myOrder++;
				}else if(myScore == s) {
					if(myCount < count) {
						myOrder++;
					}else if(myCount == count) {
						if(P-1 > i) {
							myOrder++;
							
						}
					}
				}
			}
			bw.write("#" + t + " " + myScore + " " + myOrder + '\n');
		}bw.flush();
	}
}
