import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Solution_9088_다이아몬드 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int answer = 0;
			int N = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());

			int arr[] = new int[10001];
			for (int i = 0; i < N; i++) {
				int size = Integer.parseInt(br.readLine());
				arr[size]++;
			}

			for (int i = 0; i < arr.length - K; i++) {
				int sum = 0;
				for (int k = 0; k <= K; k++) {
					sum += arr[i + k];
				}
				if (answer < sum)
					answer = sum;
			}
			bw.write("#" + t + " " + answer);
			bw.newLine();
		}
		bw.flush();
	}
}
