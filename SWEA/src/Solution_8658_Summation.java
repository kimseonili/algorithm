import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Solution_8658_Summation {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int T = Integer.parseInt(br.readLine());

		for(int t = 1; t <= T ;t ++) {
			
			StringTokenizer st = new StringTokenizer(br.readLine());
			int max = 0 ;
			int min = Integer.MAX_VALUE;
			
			while(st.hasMoreTokens()) {
				
				int n = Integer.parseInt(st.nextToken());
				int tmp = 0;
				while(n > 0) {
					tmp += n % 10;
					n /= 10;
				}
				if(tmp > max) max = tmp;
				if(tmp < min) min = tmp;
				
			}
			bw.write("#" + t + " " + max + " " + min);
			bw.newLine();
		}
		
		bw.flush();
	}

}
