import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Solution_5653_줄기세포배양 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {
			answer = 0;
			q = new LinkedList<>();
			StringTokenizer st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());
			int M = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());

			int arr[][] = new int[N + 2 * K][M + 2 * K];
			int time[][] = new int[arr.length][arr[0].length];

			for (int tt[] : time)
				Arrays.fill(tt, -1);

			for (int i = 0; i < N; i++) {
				st = new StringTokenizer(br.readLine());
				for (int j = 0; j < M; j++) {
					arr[i + K][j + K] = Integer.parseInt(st.nextToken());
					if (arr[i + K][j + K] > 0) {
						time[i + K][j + K] = 0;
						q.add(new int[] { i + K, j + K, arr[i + K][j + K], 0 });

					}
				}
			}

			bfs(arr, time, K);

//			System.out.println("TIMER");
//			for (int i[] : time)
//				System.out.println(Arrays.toString(i));
//			System.out.println();

			bw.write("#" + t + " " + answer);
			bw.newLine();
		}
		bw.flush();

	}

	static int answer;
	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
	static Queue<int[]> q;
	static int[][][] tmp;

	static void bfs(int arr[][], int time[][], int K) {

		int timeCnt = 0;
		int timer = q.size();

		int turnCnt = 1;
		tmp = new int[arr.length][arr[0].length][];
		Queue<int[]> wait = new LinkedList<>();
		while (!q.isEmpty()) {

			timeCnt++;
			int cur[] = q.poll();
			// 비활성
			cur[3]++;
			if (cur[3] <= cur[2]) {
				q.add(cur);
			} else {
				// 활성
				for (int k = 0; k < delta.length; k++) {
					int nr = cur[0] + delta[k][0];
					int nc = cur[1] + delta[k][1];

					// 이미 차지
					if (arr[nr][nc] > 0) {
						if (time[nr][nc] == turnCnt) {
							if (arr[nr][nc] < cur[2]) {
								// 먹음
								arr[nr][nc] = cur[2];
								tmp[nr][nc] = new int[] { nr, nc, cur[2], 0 };
							}

						}
					}
					// -1
					else {
						arr[nr][nc] = cur[2];
						time[nr][nc] = turnCnt;
						tmp[nr][nc] = new int[] { nr, nc, cur[2], 0 };
						wait.add(new int[] {nr, nc});
					}

				}
			}
			// 죽음체크
			if (cur[3] < cur[2] * 2 && cur[3] > cur[2]) {
				q.add(cur);
			}
			if (timer == timeCnt) {
				while(!wait.isEmpty()) {
					int w[] = wait.poll();
					q.add(tmp[w[0]][w[1]]);
				}


				turnCnt++;
				timer = q.size();
				timeCnt = 0;
				if (turnCnt == K + 1)
					break;
			}
		}
		answer = q.size();
	}

}

/*
 * 1 2 2 3 1 1 0 2
 * 
 */
