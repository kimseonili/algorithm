import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Solution_1952_������ {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int price[] = new int[4];
			for (int i = 0; i < 4; i++)
				price[i] = Integer.parseInt(st.nextToken());

			int arr[] = new int[12];
			st = new StringTokenizer(br.readLine());
			for (int i = 0; i < 12; i++)
				arr[i] = Integer.parseInt(st.nextToken());
			answer = price[3];

			comb(arr, price, new int[12], 0);
			bw.write("#" + t + " " + answer);
			bw.newLine();
		}
		bw.flush();

	}

	static int answer;

	static void comb(int arr[], int[] price, int[] c, int idx) {
		if (idx >= c.length) {
			int res = 0;
			for (int i : c)
				res += i;
//			System.out.println(Arrays.toString(c));
//			System.out.println(res);
			if (answer > res) {
				answer = res;
			}
			return;
		}

		c[idx] = price[2];
		comb(arr, price, c, idx + 3);
		c[idx] = 0;
		if (arr[idx] == 0) {
			comb(arr, price, c, idx + 1);
		}
		else {
			c[idx] = price[0] * arr[idx];
			comb(arr, price, c, idx+1);
			c[idx] = price[1];
			comb(arr, price, c, idx+1);
			c[idx] = 0;
		}

	}
}
