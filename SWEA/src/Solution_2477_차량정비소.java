import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Solution_2477_차량정비소 {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {
			answer = 0;
			StringTokenizer st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());
			int M = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());
			int A = Integer.parseInt(st.nextToken());
			int B = Integer.parseInt(st.nextToken());

			int arrN[] = new int[N];
			int arrM[] = new int[M];
			int arrK[] = new int[K];
			st = new StringTokenizer(br.readLine());
			for (int i = 0; i < N; i++) {
				arrN[i] = Integer.parseInt(st.nextToken());
			}
			st = new StringTokenizer(br.readLine());
			for (int i = 0; i < M; i++) {
				arrM[i] = Integer.parseInt(st.nextToken());
			}
			st = new StringTokenizer(br.readLine());
			for (int i = 0; i < K; i++) {
				arrK[i] = Integer.parseInt(st.nextToken());
			}
			func(arrN, arrM, arrK, A, B);
			bw.write("#" + t + " " + answer);
			bw.newLine();
		}
		bw.flush();
	}

	static int answer;

	static void func(int[] arrN, int[] arrM, int[] arrK, int A, int B) {
		Queue<int[]> q = new LinkedList<>();// 접수창구, 인덱스
		int infOrder = 0;
		int refCnt = 0;
		int time = 0;

		int inf[][] = new int[arrN.length][2];
		for (int i[] : inf)
			Arrays.fill(i, -1);
		int rep[] = new int[arrM.length];

		while (refCnt < arrK.length) {
			for (int i = 0; i < inf.length; i++) {
				inf[i][0]--;
				if (inf[i][0] <= 0) {
					if (inf[i][1] != -1 && inf[i][0] == 0) {
						q.add(new int[] { i, inf[i][1] });
					}
					if (infOrder < arrK.length && arrK[infOrder] <= time) {
						inf[i][1] = infOrder++;
						inf[i][0] = arrN[i];
						continue;
					}
				}

//				for(int j[] : inf)
//					System.out.print(Arrays.toString(j) + " ");
//				System.out.println();
			}

			for (int i = 0; i < rep.length; i++) {
				rep[i]--;
				if (rep[i] <= 0 && !q.isEmpty()) {
					rep[i] = arrM[i];
					int[] top = q.poll();
					refCnt++;
					if (i + 1 == B && top[0] + 1 == A) {
						answer += top[1] + 1;
//						System.out.println(top + 1);
					}
				}
			}

			time++;
		}
		if (answer == 0) answer = -1;
	}
}
