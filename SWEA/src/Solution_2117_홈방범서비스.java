import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Solution_2117_홈방범서비스 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {

			StringTokenizer st = new StringTokenizer(br.readLine());

			int N = Integer.parseInt(st.nextToken());
			int M = Integer.parseInt(st.nextToken());

			int arr[][] = new int[N][N];
			answer = 0;
			int totalHome = 0;
			for (int i = 0; i < N; i++) {
				st = new StringTokenizer(br.readLine());
				for (int j = 0; j < N; j++) {
					arr[i][j] = Integer.parseInt(st.nextToken());
					if (arr[i][j] == 1)
						totalHome++;
				}
			}
			bfs(arr, totalHome, M);
			
			bw.write("#" + t + " " + answer);
			bw.newLine();
		}
		bw.flush();
	}

	static int answer, r, c, kk;
	static boolean copy[][];
	static int[][] delta = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

	static boolean isRange(int nr, int nc, int N) {
		return nr >= 0 && nc >= 0 && nr < N && nc < N;
	}

	static void bfs(int arr[][], int totalHome, int M) {
		copy = new boolean[arr.length][arr.length];
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length; j++) {
				Queue<int[]> q = new LinkedList<>();
				boolean visit[][] = new boolean[arr.length][arr.length];
				visit[i][j] = true;
				q.add(new int[] { i, j, 1 });
				int homeCnt = 0;
				int K = 2;
				if (arr[i][j] == 1) {
					homeCnt++;
					if(1 <= M && answer < 1) answer = 1;
				}
				int idx = 1;
				while (!q.isEmpty()) {
					if (homeCnt == totalHome) {
						if (K * K + (K - 1) * (K - 1) <= M * homeCnt) {
							if (answer < homeCnt) {
								answer = homeCnt;
								r = i;
								c = j;
								kk = K;
//								for (int ii = 0; ii < arr.length; ii++) {
//									for (int jj = 0; jj < arr.length; jj++) {
//										copy[ii][jj] = visit[ii][jj];
//									}
//								}
//								
//								for (int ii= 0; ii < arr.length; ii++) {
//									for (int jj = 0; jj < arr.length; jj++) {
//										if (copy[ii][jj])
//											System.out.print("1 ");
//										else
//											System.out.print("0 ");
//									}
//									System.out.println();
//								}
//								System.out.println(r + ", " + c + " ::: " + kk);
//								System.out.println();
							}
						}
						break;
					}

					int cur[] = q.poll();
					if (idx != cur[2]) {

						// 계산

						if (K * K + (K - 1) * (K - 1) <= M * homeCnt) {
							if (answer < homeCnt) {
								answer = homeCnt;
								r = i;
								c = j;
								kk = K;
								
//								for (int ii = 0; ii < arr.length; ii++) {
//									for (int jj = 0; jj < arr.length; jj++) {
//										copy[ii][jj] = visit[ii][jj];
//									}
//								}
//								
//								
//								for (int ii = 0; ii < arr.length; ii++) {
//									for (int jj = 0; jj < arr.length; jj++) {
//										if (copy[ii][jj])
//											System.out.print("1 ");
//										else
//											System.out.print("0 ");
//									}
//									System.out.println();
//								}
//								System.out.println(r + ", " + c + " ::: " + kk);
//								System.out.println();
							}
						}
						idx = cur[2];
						K++;
					}
					for (int k = 0; k < delta.length; k++) {
						int nr = cur[0] + delta[k][0];
						int nc = cur[1] + delta[k][1];
						if (isRange(nr, nc, arr.length) && !visit[nr][nc]) {
							q.add(new int[] { nr, nc, cur[2] + 1 });
							visit[nr][nc] = true;
							if (arr[nr][nc] == 1)
								homeCnt++;
						}
					}

				}
			}
		}
	}
}
