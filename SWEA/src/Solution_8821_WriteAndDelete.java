import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Solution_8821_WriteAndDelete {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {
			int answer = 0;
			String input = br.readLine();
			int arr[] = new int[10];
			for(int i =  0 ; i < input.length() ; i++) {
				int num = input.charAt(i) - '0';
				if(arr[num] == 0) {
					arr[num] = 1;
				}else {
					arr[num] = 0;
				}
			}
			for(int i = 0 ; i < arr.length ; i++) {
				if(arr[i] == 1) {
					answer ++;
				}
			}
			bw.write("#" + t + " " + answer + '\n');
		}bw.flush();
	}
}
