import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Solution_4008_숫자만들기 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {

			max = Integer.MIN_VALUE;
			min = Integer.MAX_VALUE;
			int N = Integer.parseInt(br.readLine());
			StringTokenizer st = new StringTokenizer(br.readLine());
			int op[] = new int[4];

			for (int i = 0; i < 4; i++) {
				op[i] = Integer.parseInt(st.nextToken());

			}
			int num[] = new int[N];
			st = new StringTokenizer(br.readLine());
			for (int i = 0; i < N; i++) {
				num[i] = Integer.parseInt(st.nextToken());
			}
			comb(op, num, new int[N - 1], 0);
			int answer = max - min;
			bw.write("#" + t + " " + answer);
			bw.newLine();

		}
		bw.flush();

	}

	static int max;
	static int min;

	static void comb(int op[], int num[], int opArr[], int idx) {

		if (idx == opArr.length) {

			// 계산
			int res = calc(opArr, num);
			min = Math.min(min, res);
			max = Math.max(max, res);
			return;
		}
		int tmp = opArr[idx];

		if (op[0] > 0) {
			op[0]--;
			opArr[idx] = 0;
			comb(op, num, opArr, idx + 1);
			opArr[idx] = tmp;
			op[0]++;
		}
		if (op[1] > 0) {
			op[1]--;
			opArr[idx] = 1;
			comb(op, num, opArr, idx + 1);
			opArr[idx] = tmp;
			op[1]++;
		}
		if (op[2] > 0) {
			op[2]--;
			opArr[idx] = 2;
			comb(op, num, opArr, idx + 1);
			opArr[idx] = tmp;
			op[2]++;
		}
		if (op[3] > 0) {
			op[3]--;
			opArr[idx] = 3;
			comb(op, num, opArr, idx + 1);
			opArr[idx] = tmp;
			op[3]++;
		}

	}

	// 2^N * N * 50
	static int calc(int op[], int num[]) {

		int res = num[0];

		for (int i = 0; i < op.length; i++) {
			switch (op[i]) {
			case 0:
				res += num[i + 1];
				break;
			case 1:
				res -= num[i + 1];
				break;
			case 2:
				res *= num[i + 1];
				break;
			case 3:
				res /= num[i + 1];
				break;
			}
		}
		return res;

	}
}
