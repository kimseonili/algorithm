import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Solution_8931_���� {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		
		int T = Integer.parseInt(br.readLine());
		
		for(int t = 1 ; t <= T ; t++) {
			int K = Integer.parseInt(br.readLine());
			
			int arr[] = new int[K];
			int idx = -1;
			
			for(int k = 0 ; k < K ; k++) {
				int n = Integer.parseInt(br.readLine());
				if(n == 0) {
					arr[idx--] = 0;
				}else {
					arr[++idx] = n;
				}
			}
			int answer = 0;
			for(int i = 0 ; i <= idx ; i++) {
				answer += arr[i];
			}
			bw.write("#" + t + " " + answer);
			bw.newLine();
		}
		
		bw.flush();
	}
}
