import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Solution_5656_벽돌깨기 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {
			answer = Integer.MAX_VALUE;
			StringTokenizer st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());
			int W = Integer.parseInt(st.nextToken());
			int H = Integer.parseInt(st.nextToken());

			int arr[][] = new int[H][W];

			for (int i = 0; i < H; i++) {
				st = new StringTokenizer(br.readLine());
				for (int j = 0; j < W; j++) {
					arr[i][j] = Integer.parseInt(st.nextToken());
				}
			}

			comb(arr, new int[N], 0);

//			bomb(arr, new int[] { 2, 2, 6 });
			bw.write("#" + t + " " + answer);
			bw.newLine();
		}
		bw.flush();

	}

	// 중복조합
	static void comb(int arr[][], int shot[], int idx) {
		if (idx == shot.length) {
			bomb(arr, shot);
			return;
		}

		for (int i = 0; i < arr[0].length; i++) {

			shot[idx] = i;
			comb(arr, shot, idx + 1);

		}

	}

	static int delta[][] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
	static int answer;

	static void explode(int arr[][], int r, int c) {

		int tmp = arr[r][c];
		arr[r][c] = 0;
		for (int k = 0; k < delta.length; k++) {
			for (int l = 0; l < tmp; l++) {
				int nr = r + delta[k][0] * l;
				int nc = c + delta[k][1] * l;
				if (isRange(nr, nc, arr.length, arr[0].length))
					explode(arr, nr, nc);
			}
		}

	}

	static int bomb(int arr[][], int[] shot) {

		int copy[][] = new int[arr.length][arr[0].length];

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				copy[i][j] = arr[i][j];
			}
		}

		for (int i = 0; i < shot.length; i++) {

			for (int j = 0; j < copy.length; j++) {

				if (copy[j][shot[i]] > 0) {

					explode(copy, j, shot[i]);
					break;
				}

			}

			down(copy);

		}
		int res = 0;
		for (int i = 0; i < copy.length; i++) {
			for (int j = 0; j < copy[0].length; j++) {
				if (copy[i][j] > 0) {
					res++;
				}
			}
		}

		if (answer > res) {
			answer = res;


		}
//		System.out.println(Arrays.toString(shot));
//		for (int i[] : copy)
//			System.out.println(Arrays.toString(i));
//		System.out.println();
		return -1;
	}

	static void down(int arr[][]) {

		for (int i = 0; i < arr[0].length; i++) {
			int b = arr.length - 1, t = arr.length - 1;
			here: while (b > 0) {

				if (arr[b][i] == 0) {
					while (true) {

						t--;
						if (t < 0) {
							break here;
						}
						if (arr[t][i] != 0) {
							arr[b][i] = arr[t][i];
							arr[t][i] = 0;
							b--;
							t = b;
						}
					}
				} else {
					b--;
					t--;
				}

			}
		}

	}

	static boolean isRange(int nr, int nc, int R, int C) {
		return nr >= 0 && nc >= 0 && nr < R && nc < C;
	}
}
