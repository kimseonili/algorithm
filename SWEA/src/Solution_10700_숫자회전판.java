import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Solution_10700_숫자회전판 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {
			answer = 0;
			StringTokenizer st = new StringTokenizer(br.readLine());

			int N = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());

			int arr[][] = new int[N][4];

			for (int i = 0; i < N; i++) {
				st = new StringTokenizer(br.readLine());
				for (int j = 0; j < 4; j++) {
					arr[i][j] = Integer.parseInt(st.nextToken());
				}
			}

			comb(arr, new int[K], 0);
			bw.write("#" + t + " " + Integer.toString(answer));
			bw.newLine();
		}

		bw.flush();
	}
	static int answer;
	static void comb(int arr[][], int rCnt[], int idx) {
		if (idx == rCnt.length) {
			int res = calc(arr, rCnt);
//			System.out.println(res + " : " + Arrays.toString(rCnt));
			if(answer < res)
				answer = res;
			return;
		}

		for (int i = arr.length * -1; i < arr.length; i++) {
			rCnt[idx] = i;
			comb(arr, rCnt, idx + 1);
		}

	}

	static int calc(int arr[][], int rCnt[]) {
		int[][] copy = new int[arr.length][4];
		for (int i = 0; i < copy.length; i++) {
			for (int j = 0; j < 4; j++) {
				copy[i][j] = arr[i][j];
			}
		}

		for (int i = 0; i < rCnt.length; i++) {
			// rotate
			int idx = rCnt[i];
			if (rCnt[i] < 0) {
				// 역방향
				idx = (rCnt[i] + 1) * -1;
				int tmp = copy[idx][3];
				copy[idx][3] = copy[idx][0];
				copy[idx][0] = copy[idx][1];
				copy[idx][1] = copy[idx][2];
				copy[idx][2] = tmp;
			} else {
				int tmp = copy[idx][0];
				copy[idx][0] = copy[idx][3];
				copy[idx][3] = copy[idx][2];
				copy[idx][2] = copy[idx][1];
				copy[idx][1] = tmp;
			}
			if (idx < arr.length - 1) {
				copy[idx + 1][3] = copy[idx][1];
			}
			if (idx > 0) {
				copy[idx - 1][1] = copy[idx][3];
			}
//			for(int ii[] : copy)
//				System.out.println(Arrays.toString(ii));
//			System.out.println();
		}
		int res = 0;
		for (int i = 0; i < copy.length; i++) {
			res = res * 10 + copy[i][3];
		}
		res = res * 10 + copy[copy.length - 1][1];
		
		return res;
	}
}
